﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public enum Result
    {
        HomeView,
        EmployeesView,
        EmployeeDetailsView,
        AddNewEmployeeView,
        ProfessionsView,
        AddNewProfessionView,
        AddNewWorkerView,
        TreatmentsView,
        AppointmentsMainView,
        ScheduledAppointmentsView,
        ScheduleAnAppointmentView,
        AddNewClientView
    }
    public class HomeViewModel : ViewModelBase
    {
        public event EventHandler GoToOtherPageEvent;
        private DelegateCommand _goToEmployees;
        private DelegateCommand _goToProfessions;
        private DelegateCommand _goToTreatments;
        private DelegateCommand _goToAppointments;
        private Result _result;

        public Result Result
        {
            get { return _result; }
            set
            {
                if(_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
                
            }
        }

        public HomeViewModel()
        {            
            _goToEmployees = new DelegateCommand(ExecuteGoToEmployees, null);
            _goToProfessions = new DelegateCommand(ExecuteGoToProfessions, null);
            _goToTreatments = new DelegateCommand(ExecuteGoToTreatments, null);
            _goToAppointments = new DelegateCommand(ExecuteGoToAppointments, null);
        }

        private void ExecuteGoToAppointments(object obj)
        {
            Result = Result.AppointmentsMainView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToTreatments(object obj)
        {
            Result = Result.TreatmentsView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToProfessions(object obj)
        {
            Result = Result.ProfessionsView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToEmployees(object obj)
        {
            Result = Result.EmployeesView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        public DelegateCommand GoToEmployees => _goToEmployees;
        public DelegateCommand GoToProfessions => _goToProfessions;
        public DelegateCommand GoToTreatments => _goToTreatments;
        public DelegateCommand GoToAppointments => _goToAppointments;
    }
}
