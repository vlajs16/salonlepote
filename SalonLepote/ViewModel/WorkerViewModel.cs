﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Experts;
using SalonLepote.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.ValidationOperations.BrockerOperations.Treatments;
using SalonLepote.ValidationOperations.BrockerOperations.Workers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class WorkerViewModel : ViewModelBase
    {
        public event EventHandler GoToOtherPageEvent;
        private EmployeeViewModel _employee;
        private ObservableCollection<ProfessionViewModel> _professions;
        private ObservableCollection<ProfessionViewModel> _otherProfessions;
        private ObservableCollection<TreatmentViewModel> _treatments;
        private ObservableCollection<TreatmentViewModel> _otherTreatments;
        private TreatmentViewModel _selectedComboBoxTreatment;
        private TreatmentViewModel _selectedTreatment;
        private ProfessionViewModel _selectedProfession;
        private ProfessionViewModel _selectedComboboxProfession;
        private DelegateCommand _goBackToEmployeesCommand;
        private DelegateCommand _deleteWorkerCommand;
        private DelegateCommand _deleteCompleteWorkerCommand;
        private DelegateCommand _updateWorkerCommand;
        private Result _result;

        public DelegateCommand GoBackToEmployeesCommand => _goBackToEmployeesCommand;
        public DelegateCommand DeleteWorkerCommand => _deleteWorkerCommand;
        public DelegateCommand DeleteCompleteWorkerCommand => _deleteCompleteWorkerCommand;
        public DelegateCommand UpdateWorkerCommand => _updateWorkerCommand;

        public WorkerViewModel(EmployeeViewModel employee)
        {
            Employee = employee;
            ExecuteGetAllProfessionsOfEmployee(Employee.EmployeeId);
            _otherProfessions = GenerateOtherProfessions();
            _goBackToEmployeesCommand = new DelegateCommand(ExecuteGoBackToEmployees, null);
            _deleteWorkerCommand = new DelegateCommand(ExecuteDeleteWorker, null);
            _updateWorkerCommand = new DelegateCommand(ExecuteUpdateWorker, null);
            _deleteCompleteWorkerCommand = new DelegateCommand(ExecuteDeleteCompleteWorker, null);
        }

        private void InsertNewTreatmentOfProfession(ProfessionViewModel profession, TreatmentViewModel treatment)
        {
            InsertWorkerOperation.Execute(ShowEmployeeOperation.Execute((int)Employee.EmployeeId), GenerateFullExpertOperation.Execute(new Expert
            {
                Profession = new Profession { ProfessionId = profession.ProfessionId },
                Treatment = new Treatment { TreatmentId = treatment.TreatmentId }
            }));
            if (profession.Equals(SelectedComboBoxProfession))
            {
                Professions.Add(profession);
                OtherProfessions.Remove(profession);
                OtherTreatments.Remove(treatment);
                return;
            }
            Treatments.Add(treatment);
            OtherTreatments.Remove(treatment);
        }

        private void GenerateOtherTreatmentsOfSelectedProfession(ProfessionViewModel profession)
        {
            if (profession == null) return;
            OtherTreatments = new ObservableCollection<TreatmentViewModel>();
            List<Treatment> treatments = GetAllNonWorkingTreatmentsByProfessionIdAndEmployeeIdOperation.Execute(profession.ProfessionId,Employee.EmployeeId);
            foreach (var treatment in treatments)
            {
                OtherTreatments.Add(new TreatmentViewModel { TreatmentId = treatment.TreatmentId, Name = treatment.Name, Duration = treatment.Duration });
            }
        }

        private void GenerateTreatmentsByProfessionAndEmployee(ProfessionViewModel profession)
        {
            if (profession == null) return;
            Treatments = new ObservableCollection<TreatmentViewModel>();
            List<Treatment> allTreatments = GetAllWorkingTreatmentsByProfessionIdAndEmployeeIdOperation.Execute(profession.ProfessionId, Employee.EmployeeId);
            foreach (var treatment in allTreatments)
            {
                Treatments.Add(new TreatmentViewModel { TreatmentId = treatment.TreatmentId, Name = treatment.Name, Duration = treatment.Duration });
            }
        }

        private ObservableCollection<ProfessionViewModel> GenerateOtherProfessions()
        {
            List<Profession> allProfessions = GetAllProfessionsNotFromEmployeeOperation.Execute(Employee.EmployeeId);
            ObservableCollection<ProfessionViewModel> newOtherProfessions = new ObservableCollection<ProfessionViewModel>();
            foreach (var profession in allProfessions)
            {
                ProfessionViewModel newProfession = new ProfessionViewModel
                {
                    ProfessionId = profession.ProfessionId,
                    Name = profession.Name
                };
                newOtherProfessions.Add(newProfession);
            }
            return newOtherProfessions;
        }

        public void ExecuteGetAllProfessionsOfEmployee(long employeeId)
        {
            List<Profession> professions = GetAllProfessionsOfEmployeeOperation.Execute(employeeId);
            if (professions == null) return;
            ObservableCollection<ProfessionViewModel> newProfessions =
                new ObservableCollection<ProfessionViewModel>();
            foreach (var prof in professions)
            {
                ProfessionViewModel newProf = new ProfessionViewModel
                {
                    ProfessionId = prof.ProfessionId,
                    Name = prof.Name
                };
                newProfessions.Add(newProf);
            }
            Professions = newProfessions;
        }

        private void ClearData()
        {
            Employee.Name = string.Empty;
            Employee.Surname = string.Empty;
            if(_professions.Count() != 0) _professions.Clear();
            if (_otherProfessions.Count() != 0) _otherProfessions.Clear();
        }

        private void ExecuteDeleteCompleteWorker(object obj)
        {
            DeleteEmployeeOperation.Execute(Employee.EmployeeId);
            ClearData();
        }

        private void ExecuteUpdateWorker(object obj)
        {
            if (StringVlalidationOperation.Execute(_employee.Name) || StringVlalidationOperation.Execute(_employee.Surname))
            {
                UpdateEmployeeOperation.Execute(new Employee { EmployeeId = Employee.EmployeeId, Name = Employee.Name, Surname = Employee.Surname });
            }
        }

        private void ExecuteDeleteWorker(object obj)
        {
            Worker worker = new Worker
            {
                Employee = ShowEmployeeOperation.Execute((int)Employee.EmployeeId),
                Expert = ShowExperOperation.Execute(new Expert
                {
                    Profession = new Profession { ProfessionId = SelectedProfession.ProfessionId },
                    Treatment = new Treatment { TreatmentId = SelectedTreatment.TreatmentId }
                })
            };
            DeleteWorkerOperation.Execute(worker);
            _otherTreatments.Add(_selectedTreatment);
            _otherTreatments.Clear();
            _treatments.Remove(_selectedTreatment);
            if (_treatments.Count() == 0)
            {
                _otherProfessions.Add(_selectedProfession);
                _professions.Remove(_selectedProfession);
            }
            ExecuteGetAllProfessionsOfEmployee(Employee.EmployeeId);
        }

        private void ExecuteGoBackToEmployees(object obj)
        {
            Result = Result.EmployeesView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }
        
        public ProfessionViewModel SelectedProfession
        {
            get
            {
                return _selectedProfession;
            }
            set
            {
                if (value != _selectedProfession)
                {
                    _selectedProfession = value;
                    OnPropertyChanged(nameof(this._selectedProfession));
                    if (SelectedComboBoxProfession != null) SelectedComboBoxProfession = null; 
                    if(SelectedProfession!=null) GenerateTreatmentsByProfessionAndEmployee(SelectedProfession);
                    GenerateOtherTreatmentsOfSelectedProfession(SelectedProfession);
                }
            }
        }
        public TreatmentViewModel SelectedComboBoxTreatment
        {
            get
            {
                return _selectedComboBoxTreatment;
            }
            set
            {
                if (value != _selectedComboBoxTreatment)
                {
                    _selectedComboBoxTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedComboBoxTreatment));
                    if (SelectedProfession != null && SelectedComboBoxTreatment != null)
                        InsertNewTreatmentOfProfession(SelectedProfession, SelectedComboBoxTreatment);
                    if (SelectedComboBoxProfession != null && SelectedComboBoxTreatment != null)
                        InsertNewTreatmentOfProfession(SelectedComboBoxProfession, SelectedComboBoxTreatment);
                }
            }
        }
        public TreatmentViewModel SelectedTreatment
        {
            get
            {
                return _selectedTreatment;
            }
            set
            {
                if (value != _selectedTreatment)
                {
                    _selectedTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                }
            }
        }
        public ProfessionViewModel SelectedComboBoxProfession
        {
            get
            {
                return _selectedComboboxProfession;
            }
            set
            {
                if (value != _selectedComboboxProfession)
                {
                    _selectedComboboxProfession = value;
                    OnPropertyChanged(nameof(this.SelectedComboBoxProfession));
                    if (SelectedProfession != null) SelectedProfession = null;
                    if (Treatments != null) Treatments.Clear();
                    GenerateOtherTreatmentsOfSelectedProfession(SelectedComboBoxProfession);
                }
            }
        }

        public EmployeeViewModel Employee
        {
            get
            {
                return _employee;
            }
            set
            {
                if (value != _employee)
                {
                    _employee = value;
                    OnPropertyChanged(nameof(this._employee));
                }
            }
        }
        public ObservableCollection<TreatmentViewModel> OtherTreatments
        {
            get
            {
                return _otherTreatments;
            }
            set
            {
                if (value != _otherTreatments)
                {
                    _otherTreatments = value;
                    OnPropertyChanged(nameof(this.OtherTreatments));
                }
            }
        }
        public ObservableCollection<TreatmentViewModel> Treatments
        {
            get
            {
                return _treatments;
            }
            set
            {
                if (value != _treatments)
                {
                    _treatments = value;
                    OnPropertyChanged(nameof(this.Treatments));
                }
            }
        }
        public ObservableCollection<ProfessionViewModel> Professions
        {
            get
            {
                return _professions;
            }
            set
            {
                if (value != _professions)
                {
                    _professions = value;
                    OnPropertyChanged(nameof(this.Professions));
                }
            }
        }
        public ObservableCollection<ProfessionViewModel> OtherProfessions
        {
            get
            {
                return _otherProfessions;
            }
            set
            {
                if (value != _otherProfessions)
                {
                    _otherProfessions = value;
                    OnPropertyChanged(nameof(this.OtherProfessions));
                }
            }
        }
        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
            }
        }
    }
}
