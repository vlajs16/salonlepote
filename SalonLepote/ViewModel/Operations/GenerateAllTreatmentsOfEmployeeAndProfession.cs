﻿using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Treatments;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel.Operations
{
    public class GenerateAllTreatmentsOfEmployeeAndProfession
    {
        public static ObservableCollection<TreatmentViewModel> Execute(long employeeId, long professionId)
        {
            List<Treatment> treatments = GetAllTreatmentsOfWorkerOperation.Execute(employeeId, professionId);
            if (treatments == null) return null;
            ObservableCollection<TreatmentViewModel> treatmentViews = 
                new ObservableCollection<TreatmentViewModel>();
            foreach (var treat in treatments)
            {
                treatmentViews.Add(new TreatmentViewModel
                {
                    TreatmentId = treat.TreatmentId,
                    Name = treat.Name,
                    Duration = treat.Duration
                });
            }
            return treatmentViews;
        }
    }
}
