﻿using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Professions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel.Operations
{
    public class GetAllProfessionsOfEmployee
    {
        public static ObservableCollection<ProfessionViewModel> Execute(long employeeId)
        {
            List<Profession> professions = GetAllProfessionsOfEmployeeOperation.Execute(employeeId);
            if (professions == null) return null;
            ObservableCollection<ProfessionViewModel> newProfessions =
                new ObservableCollection<ProfessionViewModel>();
            foreach (var prof in professions)
            {
                ProfessionViewModel newProf = new ProfessionViewModel
                {
                    ProfessionId = prof.ProfessionId,
                    Name = prof.Name
                };
                newProfessions.Add(newProf);
            }
            return newProfessions;
        }
    }
}
