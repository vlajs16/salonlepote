﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.LogicalOperations;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace SalonLepote.ViewModel
{
    public class EmployeeViewModel : ViewModelBase
    {
        private long _employeeId;
        private string _name;
        private string _surname;
        private DelegateCommand _addNewEmployeeCommand;
        private DelegateCommand _deleteEmployeeCommand;
        private DelegateCommand _updateEmployeeCommand;
        public event EventHandler AddNewEmployee;

        public EmployeeViewModel()
        {
            _addNewEmployeeCommand = new DelegateCommand(ExecuteSaveEmoloyeeCommand, null);
            _deleteEmployeeCommand = new DelegateCommand(ExecuteDeleteEmployeeCommand, null);
            _updateEmployeeCommand = new DelegateCommand(ExecuteUpdateEmployeeCommand, null);
        }


        public long EmployeeId
        {
            get { return _employeeId; }
            set
            {
                if (_employeeId != value)
                {
                    _employeeId = value;
                    OnPropertyChanged(nameof(this.EmployeeId));
                }
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(this.Name));
                    if(StringVlalidationOperation.Execute(Name) && StringVlalidationOperation.Execute(Surname))
                        AddNewEmployee?.Invoke(this, new EventArgs());
                    Name = StringValidationOperation.Execute(Name, true);
                }
            }
        }
        public string Surname
        {
            get { return _surname; }
            set
            {
                if (_surname != value)
                {
                    _surname = value;
                    OnPropertyChanged(nameof(this.Surname));
                    if (StringVlalidationOperation.Execute(Name) && StringVlalidationOperation.Execute(Surname))
                        AddNewEmployee?.Invoke(this, new EventArgs());
                    Surname = StringValidationOperation.Execute(Surname, true);
                }
            }
        }

        public DelegateCommand AddNewEmployeeCommand => _addNewEmployeeCommand;
        public DelegateCommand DeleteEmployeeCommand => _deleteEmployeeCommand;
        public DelegateCommand UpdateEmployeeCommand => _updateEmployeeCommand;

        private void ExecuteDeleteEmployeeCommand(object obj)
        {
            DeleteEmployeeOperation.Execute(_employeeId);
        }
        private void ExecuteSaveEmoloyeeCommand(Object obj)
        {
            if (StringVlalidationOperation.Execute(_name) && StringVlalidationOperation.Execute(_surname))
                InsertEmployeeOperation.Execute(_name, _surname);
        }
        private void ExecuteUpdateEmployeeCommand(object obj)
        {
            if (StringVlalidationOperation.Execute(_name) && StringVlalidationOperation.Execute(_surname))
                UpdateEmployeeOperation.Execute(new Employee { EmployeeId = _employeeId, Name = _name, Surname = _surname });
        }
    }
}
