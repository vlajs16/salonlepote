﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatments;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Experts;
using SalonLepote.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.ValidationOperations.BrockerOperations.Treatments;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class TreatmentsViewModel : ViewModelBase
    {
        private ObservableCollection<TreatmentViewModel> _treatments;
        private TreatmentViewModel _selectedTreatment;
        private TreatmentViewModel _addNewTreatment;
        private ObservableCollection<ProfessionViewModel> _allProfessions;
        private ProfessionViewModel _selectedProfession;
        private ProfessionViewModel _selectedProfessionBox;
        private DelegateCommand _getAllTreatments;
        private DelegateCommand _deleteSelectedTreatment;
        private Result _result;

        private DelegateCommand _saveExpertCommand;
        private DelegateCommand _goToHomeView;
        private DelegateCommand _updateExpertCommand;
        public event EventHandler GoToOtherPageEvent;

        public TreatmentsViewModel()
        {
            GenerateAllProfessions();
            _getAllTreatments = new DelegateCommand(ExecuteGetAllTreatments, null);
            _deleteSelectedTreatment = new DelegateCommand(ExecuteDeleteSelectedTreatment, null);
            _goToHomeView = new DelegateCommand(ExecuteGoToHomeView, null);
            _saveExpertCommand = new DelegateCommand(ExecuteSaveExpertCommand, null);
            _updateExpertCommand = new DelegateCommand(ExecuteUpdateExpertCommand, null);
            ExecuteGetAllTreatments(null);
            GenerateNewInstanceOfAddNewTreatment();
            
        }

        private void ExecuteUpdateExpertCommand(object obj)
        {
            if (StringVlalidationOperation.Execute(SelectedTreatment.Name) && SelectedProfession != null)
            {
                UpdateTreatmentOperation.Execute(
                    new Treatment { TreatmentId = SelectedTreatment.TreatmentId, Name = SelectedTreatment.Name, Duration = 60 });
                UpdateExpertOperationOnlyProfession.Execute(SelectedProfession.ProfessionId, SelectedTreatment.TreatmentId);
            }
        }

        private void ExecuteSaveExpertCommand(object obj)
        {
            if(AddNewTreatment != null && SelectedProfessionBox != null)
            {
                long treatId = InsertTreatmentReturnIdOperation.Execute(AddNewTreatment.Name, 60);
                InsertExpertOperation.Execute(SelectedProfessionBox.ProfessionId, treatId);
                AddedTreatmentRefresh(null,null);
            }
        }

        private void GenerateAllProfessions()
        {
            List<Profession> professions = GetAllProfessionsOperation.Execute();
            ObservableCollection<ProfessionViewModel> newProfessions =
                new ObservableCollection<ProfessionViewModel>();
            foreach (Profession prof in professions)
            {
                ProfessionViewModel profession = new ProfessionViewModel
                {
                    ProfessionId = prof.ProfessionId,
                    Name = prof.Name
                };
                newProfessions.Add(profession);
            }
            AllProfessions = newProfessions;
        }

        private void GenerateNewInstanceOfAddNewTreatment()
        {
            AddNewTreatment = new TreatmentViewModel();
            SelectedProfessionBox = null;
            _addNewTreatment.OnAddNewTreatmentEvent += AddedTreatmentRefresh;
        }

        private void AddedTreatmentRefresh(object sender, EventArgs e)
        {
            ExecuteGetAllTreatments(null);
            GenerateNewInstanceOfAddNewTreatment();
        }

        private void ExecuteGoToHomeView(object obj)
        {
            Result = Result.HomeView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteDeleteSelectedTreatment(object obj)
        {
            if (_selectedTreatment != null)
            {
                DeleteTreatmentOperation.Execute((int)_selectedTreatment.TreatmentId);
                ExecuteGetAllTreatments(null);
                SelectedProfession = null;
            }
                
        }

        private void ExecuteGetAllTreatments(object obj)
        {
            List<Treatment> treatments = GetAllTreatmentsOperation.Execute();
            ObservableCollection<TreatmentViewModel> newTreatments = new ObservableCollection<TreatmentViewModel>();
            foreach (Treatment treat in treatments)
            {
                TreatmentViewModel newTreatment = new TreatmentViewModel
                {
                    TreatmentId = treat.TreatmentId,
                    Name = treat.Name,
                    Duration = treat.Duration
                };
                newTreatments.Add(newTreatment);
            }
            Treatments = newTreatments;
        }

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }

            }
        }

        public TreatmentViewModel AddNewTreatment
        {
            get { return _addNewTreatment; }
            set
            {
                if (_addNewTreatment != value)
                {
                    _addNewTreatment = value;
                    OnPropertyChanged(nameof(this.AddNewTreatment));
                }
            }
        }

        public TreatmentViewModel SelectedTreatment
        {
            get { return _selectedTreatment; }
            set
            {
                if(_selectedTreatment != value)
                {
                    _selectedTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                    if(SelectedTreatment != null)
                    {
                        SelectedProfession = GenerateSelectedProfession(SelectedTreatment.TreatmentId);
                    }
                    if(SelectedTreatment == null)
                    {
                        SelectedProfession = null;
                    }
                }
            }
        }

        private ProfessionViewModel GenerateSelectedProfession(long treatmentId)
        {
            Profession prof = GetProfessionByTreatmentIdOperation.Execute(SelectedTreatment.TreatmentId);
            if (prof == null) return null;
            ProfessionViewModel profession = new ProfessionViewModel();
            profession.Name = prof.Name;
            profession.ProfessionId = prof.ProfessionId;
            return profession;
        }

        public ObservableCollection<ProfessionViewModel> AllProfessions
        {
            get { return _allProfessions; }
            set
            {
                if(_allProfessions != value)
                {
                    _allProfessions = value;
                    OnPropertyChanged(nameof(this.AllProfessions));
                }
            }
        }

        public ProfessionViewModel SelectedProfession
        {
            get { return _selectedProfession; }
            set
            {
                if (_selectedProfession != value)
                {
                    _selectedProfession = value;
                    OnPropertyChanged(nameof(this.SelectedProfession));
                }
            }
        }

        public ProfessionViewModel SelectedProfessionBox
        {
            get { return _selectedProfessionBox; }
            set
            {
                if (_selectedProfessionBox != value)
                {
                    _selectedProfessionBox = value;
                    OnPropertyChanged(nameof(this.SelectedProfessionBox));
                }
            }
        }

        public ObservableCollection<TreatmentViewModel> Treatments
        {
            get { return _treatments; }
            set
            {
                if (_treatments != value)
                {
                    _treatments = value;
                    OnPropertyChanged(nameof(this.Treatments));
                }
            }
        }

        public DelegateCommand GetAllTreatments => _getAllTreatments;
        public DelegateCommand DeleteSelectedTreatment => _deleteSelectedTreatment;
        public DelegateCommand GoToHomeView => _goToHomeView;
        public DelegateCommand SaveExpertCommand => _saveExpertCommand;
        public DelegateCommand UpdateExpertCommand => _updateExpertCommand;
    }
}
