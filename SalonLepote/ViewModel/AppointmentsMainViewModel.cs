﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class AppointmentsMainViewModel : ViewModelBase
    {
        private Result _result;
        private DelegateCommand _goToHomeView;
        private DelegateCommand _goToScheduleAnAppointment;
        private DelegateCommand _goToScheduledAppointments;
        public event EventHandler GoToOtherPageEvent;

        public DelegateCommand GoToHomeView => _goToHomeView;
        public DelegateCommand GoToShceduledAppointments => _goToScheduledAppointments;
        public DelegateCommand GoToScheduleAnAppointment => _goToScheduleAnAppointment;

        public AppointmentsMainViewModel()
        {
            _goToHomeView = new DelegateCommand(ExecuteGoTOHomeView, null);
            _goToScheduleAnAppointment = new DelegateCommand(ExecuteGoToScheduleAnAppointment, null);
            _goToScheduledAppointments = new DelegateCommand(ExecuteGoToScheduledAppointment, null);
        }

        private void ExecuteGoToScheduledAppointment(object obj)
        {
            Result = Result.ScheduledAppointmentsView;
            //Result = Result.AddNewClientView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToScheduleAnAppointment(object obj)
        {
            Result = Result.ScheduleAnAppointmentView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoTOHomeView(object obj)
        {
            Result = Result.HomeView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }

            }
        }
    }
}
