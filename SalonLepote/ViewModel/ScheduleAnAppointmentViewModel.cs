﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Clients;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements;
using SalonLepote.Konzolna.ValidationOperations.LogicalOperations;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Appointments;
using SalonLepote.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.ValidationOperations.BrockerOperations.Workers;
using SalonLepote.ViewModel.Operations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class ScheduleAnAppointmentViewModel : ViewModelBase
    {
        private DateTime _selectedDate;
        private DateTime _selectedTime;
        private ObservableCollection<DateTime> _freeTime;
        private ObservableCollection<DateTime> _bookedTime; 
        private EmployeeViewModel _selectedEmployee;
        private ObservableCollection<EmployeeViewModel> _allEmployees;
        private TreatmentViewModel _selectedTreatment;
        private ObservableCollection<TreatmentViewModel> _allTreatments;
        private AddNewClientViewModel _selectedClient;
        private ObservableCollection<AddNewClientViewModel> _allClients;
        private Result _result;

        public event EventHandler GoToOtherPageEvent;

        private DelegateCommand _goToAddClientsCommand;
        private DelegateCommand _goBackToMainAppointmentsCommand;
        private DelegateCommand _saveAppointmentCommand;

        public DelegateCommand GoToAddClientsCommand => _goToAddClientsCommand;
        public DelegateCommand GoBackToMainAppoinmtentsCommand => _goBackToMainAppointmentsCommand;
        public DelegateCommand SaveAppointmentCommand => _saveAppointmentCommand;

        public ScheduleAnAppointmentViewModel()
        {
            SelectedDate = DateTime.Now;
            GenerateAllTreatments();
            GenerateAllClients();
            _goToAddClientsCommand = new DelegateCommand(ExecuteGoToAddClientsCommand, null);
            _goBackToMainAppointmentsCommand = new DelegateCommand(ExecuteGoToMainAppointmentsCommand, null);
            _saveAppointmentCommand = new DelegateCommand(ExecuteSaveAppointmentCommand, null);
        }

        public void GenerateEmployees()
        {
            if(SelectedTreatment != null)
            {
                List<Employee> employees = GetEmployeesByTreatmentOperation.Execute(SelectedTreatment.TreatmentId);
                if(employees != null)
                {
                    ObservableCollection<EmployeeViewModel> newEmployees = new ObservableCollection<EmployeeViewModel>();
                    foreach (var emp in employees)
                    {
                        newEmployees.Add(new EmployeeViewModel
                        {
                            EmployeeId = emp.EmployeeId,
                            Name = emp.Name,
                            Surname = emp.Surname
                        });
                    }
                    AllEmployees = newEmployees;
                }
            }
        }

        private void GenerateAllTreatments()
        {
            List<Treatment> treatments = GetAllTreatmentsOperation.Execute();
            ObservableCollection<TreatmentViewModel> newTreatments = new ObservableCollection<TreatmentViewModel>();
            foreach (Treatment treat in treatments)
            {
                TreatmentViewModel newTreatment = new TreatmentViewModel
                {
                    TreatmentId = treat.TreatmentId,
                    Name = treat.Name,
                    Duration = treat.Duration
                };
                newTreatments.Add(newTreatment);
            }
            AllTreatments = newTreatments;
        }

        private void GenerateFreeTime()
        {
            if(SelectedDate != null)
            {
                FreeTime = new ObservableCollection<DateTime>();
                int startHour = 9;
                int endHour = 20;
                int day = SelectedDate.Day;
                int month = SelectedDate.Month;
                int year = SelectedDate.Year;

                for (int i = startHour; i < endHour; i++)
                {
                    DateTime date = new DateTime(year, month, day, i, 0, 0);
                    if(date > DateTime.Now)
                        FreeTime.Add(date);  
                }

                if(BookedTime != null)
                {
                    foreach (var date in BookedTime)
                    {
                        if (FreeTime.Contains(date))
                            FreeTime.Remove(date);
                    }
                }
            }
        }

        private void GenerateBookedTime()
        {
            if (SelectedDate == null && SelectedEmployee == null) return;
            List<Appointment> appointments = GetAppointmentsForSpecificDateAndEmployee.Execute(SelectedEmployee.EmployeeId, SelectedDate);
            if (appointments == null) return;
            ObservableCollection<DateTime> newTime = new ObservableCollection<DateTime>();
            foreach (var app in appointments)
            {
                newTime.Add(app.StartTime);
            }
            BookedTime = newTime;
            GenerateFreeTime();
        }

        private void GenerateAllClients()
        {
            List<Client> clients = GetAllClientsOperation.Execute();
            if(clients != null)
            {
                ObservableCollection<AddNewClientViewModel> newClients = new ObservableCollection<AddNewClientViewModel>();
                foreach (var client in clients)
                {
                    newClients.Add(new AddNewClientViewModel
                    {
                        ClientId = client.ClientId,
                        Name = client.Name,
                        Surname = client.Surname,
                        PhoneNumber = client.PhoneNumber
                    });
                }
                _allClients = newClients;
            }
        }

        private void TreatmentListSetter(ref ObservableCollection<TreatmentViewModel> newTreatments,
            ObservableCollection<TreatmentViewModel> loadedTreatments)
        {
            if (loadedTreatments == null) return;
            foreach (var treat in loadedTreatments)
            {
                newTreatments.Add(treat);
            }
        }

        private void ExecuteSaveAppointmentCommand(object obj)
        {
            if (SelectedDate == null) return;
            if (SelectedClient == null) return;
            if (SelectedEmployee == null) return;
            if (SelectedTreatment == null) return;
            long professionId = GenerateProfessionId(SelectedEmployee.EmployeeId,
                    SelectedTreatment.TreatmentId);
            if (professionId == -1) return;

            InsertAppointmentOperation.Execute( new Worker
                {
                    Employee = new Employee { EmployeeId = SelectedEmployee.EmployeeId},
                    Expert = new Expert { Profession = new Profession { ProfessionId = professionId }, Treatment = new Treatment { TreatmentId = SelectedTreatment.TreatmentId } }
                }, 
                new Client { ClientId = SelectedClient.ClientId}, SelectedTime, 
                GenerateEndTime.Execute(SelectedTime, ShowTreatmentOperation.Execute((int) SelectedTreatment.TreatmentId)));
            FreeTime.Remove(SelectedTime);
        }

        private long GenerateProfessionId(long employeeId, long treatmentId)
        {
            Worker worker = ReturnSpecificWorkerOperation.Execute(employeeId, treatmentId);
            if (worker == null) return -1;
            return worker.Expert.Profession.ProfessionId;
        }

        private void ExecuteGoToMainAppointmentsCommand(object obj)
        {
            Result = Result.AppointmentsMainView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToAddClientsCommand(object obj)
        {
            Result = Result.AddNewClientView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }

            }
        }

        public ObservableCollection<AddNewClientViewModel> AllClients
        {
            get { return _allClients; }
            set
            {
                if (_allClients != value)
                {
                    _allClients = value;
                    OnPropertyChanged(nameof(this.AllClients));

                }
            }
        }

        public AddNewClientViewModel SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                if (_selectedClient != value)
                {
                    _selectedClient = value;
                    OnPropertyChanged(nameof(this.SelectedClient));

                }
            }
        }

        public ObservableCollection<TreatmentViewModel> AllTreatments
        {
            get { return _allTreatments; }
            set
            {
                if (_allTreatments != value)
                {
                    _allTreatments = value;
                    OnPropertyChanged(nameof(this.AllTreatments));

                }
            }
        }

        public TreatmentViewModel SelectedTreatment
        {
            get { return _selectedTreatment; }
            set
            {
                if (_selectedTreatment != value)
                {
                    _selectedTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                    if(_selectedTreatment!= null)
                    {
                        GenerateEmployees();
                    }
                }
            }
        }

        public ObservableCollection<EmployeeViewModel> AllEmployees
        {
            get { return _allEmployees; }
            set
            {
                if (_allEmployees != value)
                {
                    _allEmployees = value;
                    OnPropertyChanged(nameof(this.AllEmployees));

                }
            }
        }

        public EmployeeViewModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set
            {
                if (_selectedEmployee != value)
                {
                    _selectedEmployee = value;
                    OnPropertyChanged(nameof(this.SelectedEmployee));
                    if (SelectedEmployee != null)
                    {
                        if(SelectedDate != null)
                        {
                            GenerateBookedTime();
                        }
                    }
                }
            }
        }

        public ObservableCollection<DateTime> BookedTime
        {
            get { return _bookedTime; }
            set
            {
                if (_bookedTime != value)
                {
                    _bookedTime = value;
                    OnPropertyChanged(nameof(this.BookedTime));

                }
            }
        }

        public ObservableCollection<DateTime> FreeTime
        {
            get { return _freeTime; }
            set
            {
                if (_freeTime != value)
                {
                    _freeTime = value;
                    OnPropertyChanged(nameof(this.FreeTime));

                }
            }
        }

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if (_selectedDate != value)
                {
                    _selectedDate = value;
                    OnPropertyChanged(nameof(this.SelectedDate));
                    if(SelectedDate != null && SelectedEmployee != null)
                    {
                        GenerateBookedTime();
                    }

                }
            }
        }

        public DateTime SelectedTime
        {
            get { return _selectedTime; }
            set
            {
                if(_selectedTime != value)
                {
                    _selectedTime = value;
                    OnPropertyChanged(nameof(this.SelectedTime));
                }
            }
        }
    }
}
