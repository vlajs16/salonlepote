﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.LogicalOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class ProfessionViewModel : ViewModelBase
    {
        
        private long _professionId;
        private string _name;
        private Result _result;
        private DelegateCommand _addNewProfessionCommand;
        private DelegateCommand _deleteProfessionCommand;
        private DelegateCommand _updateProfessionCommand;
        private DelegateCommand _clearNameCommand;


        private DelegateCommand _goToBackProfessions;
        public event EventHandler GoToOtherPageEvent;

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
            }
        }

        public ProfessionViewModel()
        {
            _addNewProfessionCommand = new DelegateCommand(ExecuteAddNewProfession, null);
            _deleteProfessionCommand = new DelegateCommand(ExecuteDeleteProfession, null);
            _updateProfessionCommand = new DelegateCommand(ExecuteUpdateProfession, null);
            _goToBackProfessions = new DelegateCommand(ExecuteGoBackToProfessions, null);
            _clearNameCommand = new DelegateCommand(ExecuteClearNameCommand, null);
        }

        private void ExecuteClearNameCommand(object obj)
        {
            Name = string.Empty;
        }

        private void ExecuteGoBackToProfessions(object obj)
        {
            Result = Result.ProfessionsView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteUpdateProfession(object obj)
        {
            if (StringVlalidationOperation.Execute(_name))
                UpdateProfessionOperation.Execute(new Profession { ProfessionId = this.ProfessionId, Name = this.Name });
        }

        private void ExecuteDeleteProfession(object obj)
        {
            DeleteProfessionOperation.Execute((int)_professionId);
        }

        private void ExecuteAddNewProfession(object obj)
        {
            if (StringVlalidationOperation.Execute(_name))
            {
                InsertProfessionOperation.Execute(_name);
                ExecuteClearNameCommand(null);
            }
                
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(this.Name));
                    Name = StringValidationOperation.Execute(Name, false);
                }
            }
        }
        public long ProfessionId
        {
            get { return _professionId; }
            set
            {
                if (_professionId != value)
                {
                    _professionId = value;
                    OnPropertyChanged(nameof(this.ProfessionId));
                }
            }
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            ProfessionViewModel newProfessionVM = (ProfessionViewModel)obj;
            if (ProfessionId.Equals(newProfessionVM.ProfessionId) && Name.Equals(newProfessionVM.Name))
                return true;
            return false;
        }

        public override string ToString()
        {
            return _name;
        }

        public DelegateCommand AddProfessionCommand => _addNewProfessionCommand;
        public DelegateCommand DeleteProfessionCommand => _deleteProfessionCommand;
        public DelegateCommand UpdateProfessionCommand => _updateProfessionCommand;
        public DelegateCommand GoBackToProfessionsCommand => _goToBackProfessions;
        public DelegateCommand ClearNameCommand => _clearNameCommand;
    }
}
