﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Experts;
using SalonLepote.ValidationOperations.BrockerOperations.Treatments;
using SalonLepote.ValidationOperations.BrockerOperations.Workers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SalonLepote.ViewModel
{
    public class AddNewWorkerViewModel : ViewModelBase
    {
        private EmployeeViewModel _newEmployee;
        private ObservableCollection<ProfessionViewModel> _professions;
        private ObservableCollection<ProfessionViewModel> _newProfessions;
        private ObservableCollection<TreatmentViewModel> _treatments;
        private ObservableCollection<TreatmentViewModel> _newTreatments;
        private ProfessionViewModel _selectedProfession;
        private ProfessionViewModel _selectedNewProfession;
        private TreatmentViewModel _selectedTreatment;
        private TreatmentViewModel _selectedNewTreatment;
        private DelegateCommand _deleteCommand;
        private DelegateCommand _clearEverythingCommand;
        private DelegateCommand _goBackToEmployeesView;
        public DelegateCommand _addProfessionToSelectedCommand;
        private Result _result;
        private bool check;
        public event EventHandler GoToOtherPageEvent;

        public DelegateCommand DeleteCommand => _deleteCommand;
        public DelegateCommand ClearEverythingCommand => _clearEverythingCommand;
        public DelegateCommand GoBackToEmployeesView => _goBackToEmployeesView;

        public AddNewWorkerViewModel()
        {
            check = true;
            _newEmployee = new EmployeeViewModel();
            NewEmployee.AddNewEmployee += InsertEmployee;
            GenerateAllProfessions();
            _professions = new ObservableCollection<ProfessionViewModel>();
            _deleteCommand = new DelegateCommand(ExecuteDeleteCommand, null);
            _clearEverythingCommand = new DelegateCommand(ExecuteClearEverythingCommand, null);
            _goBackToEmployeesView = new DelegateCommand(ExecuteGoBackToEmployeeCommand, null);
        }

        private void InsertEmployee(object sender, EventArgs e)
        {
            if (check)
            {
                NewEmployee.EmployeeId = InsertEmployeeReturnIdOperation.Execute(NewEmployee.Name, NewEmployee.Surname);
                check = false;
            }
            else UpdateEmployeeOperation.Execute(new Employee { EmployeeId = NewEmployee.EmployeeId, Name = NewEmployee.Name, Surname = NewEmployee.Surname});
        }

        private void ExecuteGoBackToEmployeeCommand(object obj)
        {
            Result = Result.EmployeesView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteClearEverythingCommand(object obj)
        {
            NewEmployee.Name = "";
            NewEmployee.Surname = "";
            if (Professions != null) Professions.Clear();
            if (Treatments != null) Treatments.Clear();
            GenerateAllProfessions();
        }

        private void GenerateAllProfessions()
        {
            List<Profession> allProfessions = GetAllProfessionsOperation.Execute();
            NewProfessions = new ObservableCollection<ProfessionViewModel>();
            foreach (Profession prof in allProfessions)
            {
                ProfessionViewModel profession = new ProfessionViewModel
                {
                    ProfessionId = prof.ProfessionId,
                    Name = prof.Name
                };
                NewProfessions.Add(profession);
            }
        }

        private void ExecuteDeleteCommand(object obj)
        {
            if (SelectedProfession != null)
            {
                if (SelectedTreatment != null)
                {
                    DeleteWorkerOperation.Execute(new Worker
                    {
                        Employee = new Employee { EmployeeId = NewEmployee.EmployeeId},
                        Expert = new Expert
                        {
                            Profession = new Profession { ProfessionId = SelectedProfession.ProfessionId},
                            Treatment = new Treatment { TreatmentId = SelectedTreatment.TreatmentId}
                        }
                    });
                    Treatments.Remove(SelectedTreatment);
                    return;
                }
                List<Worker> workers = GetAllWorkersByProfessionIdAndEmployeeIdOperation.Execute(SelectedProfession.ProfessionId, NewEmployee.EmployeeId);
                NewProfessions.Add(SelectedProfession);
                Professions.Remove(SelectedProfession);
                Treatments.Clear();
                foreach (var worker in workers)
                {
                    DeleteWorkerOperation.Execute(worker);
                }
            }
        }

        private void GenerateTreatmentsOfProfession(ProfessionViewModel profession)
        {
            List<Treatment> allTreatments = GetAllTreatmentsOfProfessionOperation.Execute(profession.ProfessionId);
            NewTreatments = new ObservableCollection<TreatmentViewModel>();
            foreach (var treat in allTreatments)
            {
                NewTreatments.Add(new TreatmentViewModel() { TreatmentId = treat.TreatmentId, Name = treat.Name, Duration = treat.Duration });
            }
        }

        private void GenerateWorkingTreatmentsOfProfession(ProfessionViewModel profession)
        {
            List<Treatment> allTreatments = GetAllWorkingTreatmentsByProfessionIdAndEmployeeIdOperation.Execute(profession.ProfessionId, NewEmployee.EmployeeId);
            Treatments = new ObservableCollection<TreatmentViewModel>();
            foreach (var treat in allTreatments)
            {
                Treatments.Add(new TreatmentViewModel() { TreatmentId = treat.TreatmentId, Name = treat.Name, Duration = treat.Duration });
            }
        }

        private void InsertNewTreatmentOfProfession(ProfessionViewModel profession, TreatmentViewModel treatment)
        {
            if (profession == null) return;
            InsertWorkerOperation.Execute(ShowEmployeeOperation.Execute((int)NewEmployee.EmployeeId), GenerateFullExpertOperation.Execute(new Expert
            {
                Profession = new Profession { ProfessionId = profession.ProfessionId },
                Treatment = new Treatment { TreatmentId = treatment.TreatmentId }
            }));
            if (profession.Equals(SelectedNewProfession))
            {
                Professions.Add(profession);
                NewProfessions.Remove(profession);
                NewTreatments.Remove(treatment);
                return;
            }
            Treatments.Add(treatment);
            NewTreatments.Remove(treatment);
        }

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }

            }
        }
        public ObservableCollection<TreatmentViewModel> NewTreatments
        {
            get { return _newTreatments; }
            set
            {
                if (_newTreatments != value)
                {
                    _newTreatments = value;
                    OnPropertyChanged(nameof(this.NewTreatments));
                }
            }
        }
        public ObservableCollection<TreatmentViewModel> Treatments
        {
            get { return _treatments; }
            set
            {
                if (_treatments != value)
                {
                    _treatments = value;
                    OnPropertyChanged(nameof(this.Treatments));
                }
            }
        }
        public ObservableCollection<ProfessionViewModel> NewProfessions
        {
            get { return _newProfessions; }
            set
            {
                if(_newProfessions != value)
                {
                    _newProfessions = value;
                    OnPropertyChanged(nameof(this.NewProfessions));
                }
            }
        }
        public TreatmentViewModel SelectedNewTreatment
        {
            get { return _selectedNewTreatment; }
            set
            {
                if (_selectedNewTreatment != value)
                {
                    _selectedNewTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedNewTreatment));
                    if (SelectedProfession != null && SelectedNewTreatment != null)
                    {
                        InsertNewTreatmentOfProfession(SelectedProfession, SelectedNewTreatment);
                    }
                    if (SelectedNewProfession != null && SelectedNewTreatment != null)
                    {

                        InsertNewTreatmentOfProfession(SelectedNewProfession, SelectedNewTreatment);
                    }
                }
            }
        }
        public TreatmentViewModel SelectedTreatment
        {
            get { return _selectedTreatment; }
            set
            {
                if (_selectedTreatment != value)
                {
                    _selectedTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                }
            }
        }
        public ProfessionViewModel SelectedNewProfession
        {
            get { return _selectedNewProfession; }
            set
            {
                if (_selectedNewProfession != value)
                {
                    _selectedNewProfession = value;
                    OnPropertyChanged(nameof(this.SelectedNewProfession));
                    if (SelectedProfession != null)
                    {
                        SelectedProfession = null;
                        if (Treatments != null) Treatments.Clear();
                    }
                    if (SelectedNewProfession != null) GenerateTreatmentsOfProfession(SelectedNewProfession);
                }
            }
        }
        public ProfessionViewModel SelectedProfession
        {
            get { return _selectedProfession; }
            set
            {
                if (_selectedProfession != value)
                {
                    _selectedProfession = value;
                    OnPropertyChanged(nameof(this.SelectedProfession));
                    if (SelectedNewProfession != null) SelectedNewProfession = null;
                    if (SelectedProfession != null) GenerateWorkingTreatmentsOfProfession(SelectedProfession);
                }
            }
        }

        public ObservableCollection<ProfessionViewModel> Professions
        {
            get { return _professions; }
            set
            {   
                if(_professions != value)
                {
                    _professions = value;
                    OnPropertyChanged(nameof(this.Professions));
                }
            }
        }
        public EmployeeViewModel NewEmployee
        {
            get { return _newEmployee; }
            set
            {
                if(_newEmployee != value)
                {
                    _newEmployee = value;
                    OnPropertyChanged(nameof(this.NewEmployee));
                }
            }
        }
    }
}
