﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SalonLepote.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ViewModelBase _mainContentViewModel;
        private HomeViewModel _homeViewModel;
        private EmployeesViewModel _employeesViewModel;
        private EmployeeViewModel _employeeViewModel;
        private ProfessionsViewModel _professionsViewModel;
        private ProfessionViewModel _professionViewModel;
        private WorkerViewModel _workerViewModel;
        private TreatmentsViewModel _treatmentsViewModel;
        private AddNewWorkerViewModel _addNewWorkerViewModel;
        private AppointmentsMainViewModel _appointmentsMainViewModel;
        private AddNewClientViewModel _addNewClientViewModel;
        private ScheduleAnAppointmentViewModel _scheduleAnAppointmentViewModel;
        private ScheduledAppointmentsViewModel _scheduledAppointmentsViewModel;

        public MainViewModel()
        {        
            _homeViewModel = new HomeViewModel();
            _homeViewModel.GoToOtherPageEvent += OnHomeViewModelFinished;
            
            SetHomeViewMode();
        }

        private void GenerateScheduleAnAppointmentViewModel()
        {
            _scheduleAnAppointmentViewModel = new ScheduleAnAppointmentViewModel();
            _scheduleAnAppointmentViewModel.GoToOtherPageEvent += OnScheduleAnAppointmentViewModelFinished;
        }

        private void GenerateScheduledAppointmentsViewModel()
        {
            _scheduledAppointmentsViewModel = new ScheduledAppointmentsViewModel();
            _scheduledAppointmentsViewModel.GoToOtherPageEvent += OnScheduledAppointmetnsViewModelFinished;
        }

        private void GenerateAddNewClientViewModel()
        {
            _addNewClientViewModel = new AddNewClientViewModel();
            _addNewClientViewModel.GoToOtherPageEvent += OnAddNewClientModelFinished;
        }
        private void GenerateAppointmentsMainViewModel()
        {
            _appointmentsMainViewModel = new AppointmentsMainViewModel();
            _appointmentsMainViewModel.GoToOtherPageEvent += OnAppointmentsMainViewModelFinished;
        }

        private void GenerateAddNewWorkerView()
        {
            _addNewWorkerViewModel = new AddNewWorkerViewModel();
            _addNewWorkerViewModel.GoToOtherPageEvent += OnGenerateAddNewWorkerModelFinished;
        }

        private void GenerateTreatmentsViewModel()
        {
            _treatmentsViewModel = new TreatmentsViewModel();
            _treatmentsViewModel.GoToOtherPageEvent += OnTreatmentsViewModelFinished;
        }

        private void GenerateEmployeesViewModel()
        {
            _employeesViewModel = new EmployeesViewModel();
            _employeesViewModel.GoToOtherPageEvents += OnEmployeesViewModelFinished;
        }
        private void GenerateEmployeeViewModel()
        {
            _employeeViewModel = new EmployeeViewModel();
        }

        private void GenerateWorkerViewModel(EmployeeViewModel employee)
        {
            _workerViewModel = new WorkerViewModel(employee);
            _workerViewModel.GoToOtherPageEvent += OnWorkerViewModelFinished;
        }

        private void GenerateProfessionsViewModel()
        {
            _professionsViewModel = new ProfessionsViewModel();
            _professionsViewModel.GoToOtherPageEvent += OnProfessionsViewModelFinished;
        }

        private void GenerateProfessionViewModel()
        {
            _professionViewModel = new ProfessionViewModel();
            _professionViewModel.GoToOtherPageEvent += OnProfessionViewModelFinished;
        }


        private void OnScheduleAnAppointmentViewModelFinished(object sender, EventArgs e)
        {
            switch (_scheduleAnAppointmentViewModel.Result)
            {
                case Result.AppointmentsMainView:
                    {
                        MainContentViewModel = _appointmentsMainViewModel;
                        break;
                    }
                case Result.AddNewClientView:
                    {
                        GenerateAddNewClientViewModel();
                        MainContentViewModel = _addNewClientViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnScheduledAppointmetnsViewModelFinished(object sender, EventArgs e)
        {
            switch (_scheduledAppointmentsViewModel.Result)
            {
                case Result.AppointmentsMainView:
                    {
                        MainContentViewModel = _appointmentsMainViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }
        private void OnAddNewClientModelFinished(object sender, EventArgs e)
        {
            switch (_addNewClientViewModel.Result)
            {
                case Result.AppointmentsMainView:
                    {
                        MainContentViewModel = _appointmentsMainViewModel;
                        break;
                    }
                case Result.ScheduleAnAppointmentView:
                    {
                        GenerateScheduleAnAppointmentViewModel();
                        MainContentViewModel = _scheduleAnAppointmentViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnAppointmentsMainViewModelFinished(object sender, EventArgs e)
        {
            switch (_appointmentsMainViewModel.Result)
            {
                case Result.HomeView:
                    {
                        MainContentViewModel = _homeViewModel;
                        break;
                    }
                case Result.ScheduleAnAppointmentView:
                    {
                        GenerateScheduleAnAppointmentViewModel();
                        MainContentViewModel = _scheduleAnAppointmentViewModel;
                        break;
                    }
                case Result.ScheduledAppointmentsView:
                    {
                        GenerateScheduledAppointmentsViewModel();
                        MainContentViewModel = _scheduledAppointmentsViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }


        private void OnGenerateAddNewWorkerModelFinished(object sender, EventArgs e)
        {
            switch(_addNewWorkerViewModel.Result)
            {
                case Result.EmployeesView:
                    {
                        GenerateEmployeesViewModel();
                        MainContentViewModel = _employeesViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnTreatmentsViewModelFinished(object sender, EventArgs e)
        {
            switch (_treatmentsViewModel.Result)
            {
                case Result.HomeView:
                    {
                        MainContentViewModel = _homeViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnProfessionViewModelFinished(object sender, EventArgs e)
        {
            switch (_professionViewModel.Result)
            {
                case Result.ProfessionsView:
                    {
                        GenerateProfessionsViewModel();
                        MainContentViewModel = _professionsViewModel;
                        break;
                    }
                default:MainContentViewModel = null;break;
            }
        }

        private void OnProfessionsViewModelFinished(object sender, EventArgs e)
        {
            switch (_professionsViewModel.Result)
            {                
                case Result.AddNewProfessionView:
                    {
                        GenerateProfessionViewModel();
                        MainContentViewModel = _professionViewModel;
                        break;
                    }
                case Result.HomeView:
                    {
                        MainContentViewModel = _homeViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnEmployeesViewModelFinished(object sender, EventArgs e)
        {
            switch (_employeesViewModel.Result)
            {
                case Result.EmployeeDetailsView:
                    {
                        GenerateEmployeeViewModel();
                        GenerateWorkerViewModel(_employeesViewModel.SelectedEmoloyee);
                        MainContentViewModel = _workerViewModel;
                        break;
                    }
                case Result.HomeView:
                    {
                        MainContentViewModel = _homeViewModel;
                        break;
                    }
                case Result.AddNewWorkerView:
                    {
                        GenerateAddNewWorkerView();
                        MainContentViewModel = _addNewWorkerViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void OnHomeViewModelFinished(object sender, EventArgs e)
        {
            switch (_homeViewModel.Result)
            {
                case Result.EmployeesView:
                    {
                        GenerateEmployeesViewModel();
                        MainContentViewModel = _employeesViewModel;
                        break;
                    }
                case Result.ProfessionsView:
                    {
                        GenerateProfessionsViewModel();
                        MainContentViewModel = _professionsViewModel;
                        break;
                    }
                case Result.TreatmentsView:
                    {
                        GenerateTreatmentsViewModel();
                        MainContentViewModel = _treatmentsViewModel;
                        break;
                    }
                case Result.AppointmentsMainView:
                    {
                        GenerateAppointmentsMainViewModel();
                        MainContentViewModel = _appointmentsMainViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }
        private void OnWorkerViewModelFinished(object sender, EventArgs e)
        {
            switch (_workerViewModel.Result)
            {
                case Result.EmployeesView:
                    {
                        GenerateEmployeesViewModel();
                        MainContentViewModel = _employeesViewModel;
                        break;
                    }
                default: MainContentViewModel = null; break;
            }
        }

        private void SetHomeViewMode()
        {
            MainContentViewModel = _homeViewModel;
        }

        public ViewModelBase MainContentViewModel
        {
            get
            {
                return _mainContentViewModel;
            }
            set
            {
                if(_mainContentViewModel != value)
                {
                    _mainContentViewModel = value;
                    OnPropertyChanged(nameof(this.MainContentViewModel));
                }
            }
        }
    }
}
