﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatments;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.LogicalOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class TreatmentViewModel : ViewModelBase
    {
        private long _treatmentId;
        private string _name;
        private int _duration;
        private DelegateCommand _updateTreatment;
        private DelegateCommand _addNewTreatment;
        private DelegateCommand _deleteTreatment;

        public event EventHandler OnAddNewTreatmentEvent;

        public TreatmentViewModel()
        {
            _updateTreatment = new DelegateCommand(ExecuteUpdateTreatment,null);
            _addNewTreatment = new DelegateCommand(ExecuteAddNewTreatment, null);
            _deleteTreatment = new DelegateCommand(ExecuteDeleteTreatment, null);
        }

        private void ExecuteDeleteTreatment(object obj)
        {
            DeleteTreatmentOperation.Execute((int)_treatmentId);
        }

        private void ExecuteAddNewTreatment(object obj)
        {
            if (StringVlalidationOperation.Execute(_name))
            {
                InsertTreatmentOperation.Execute(_name, _duration);
                OnAddNewTreatmentEvent?.Invoke(this, new EventArgs());
            }
                

        }

        public void ExecuteAddNewTreatment(TreatmentViewModel treat)
        {
            if (StringVlalidationOperation.Execute(treat.Name))
            {
                InsertTreatmentOperation.Execute(treat.Name, 60);
            }
        }

        private void ExecuteUpdateTreatment(object obj)
        {
            if (StringVlalidationOperation.Execute(_name))
            {
                UpdateTreatmentOperation.Execute(
                    new Treatment { TreatmentId = _treatmentId, Name = _name, Duration = 60});
            }
        }

        public int Duration
        {
            get { return _duration; }
            set
            {
                if(_duration != value)
                {
                    _duration = value;
                    OnPropertyChanged(nameof(this.Duration));
                }
            }
        }


        public string Name
        {
            get { return _name; }
            set
            {
                if(_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(this.Name));
                    Name = StringValidationOperation.Execute(Name, false);
                }
            }
        }


        public long TreatmentId
        {
            get { return _treatmentId; }
            set
            {
                if(_treatmentId != value)
                {
                    _treatmentId = value;
                    OnPropertyChanged(nameof(this.TreatmentId));
                }
            }
        }

        public DelegateCommand UpdateTreatment => _updateTreatment;
        public DelegateCommand DeleteTreatment => _deleteTreatment;
        public DelegateCommand AddNewTreatment => _addNewTreatment;
    }
}
