﻿using SalonLepote.Konzolna.ValidationOperations;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations;
using SalonLepote.ValidationOperations.LogicalOperations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel 
{
    public class AddNewClientViewModel : ViewModelBase
    {
        public event EventHandler GoToOtherPageEvent;
        private long _clientId;
        private string _name;
        private string _surname;
        private string _phoneNumber;
        private Result _result;
        private DelegateCommand _goBackCommand;
        private DelegateCommand _addClientCommand;
        private DelegateCommand _clearCommand;

        public DelegateCommand GoBackCommand => _goBackCommand;
        public DelegateCommand AddClientCommand => _addClientCommand;
        public DelegateCommand ClearCommand => _clearCommand;

        public AddNewClientViewModel()
        {
            _goBackCommand = new DelegateCommand(ExecuteGoBack,null);
            _addClientCommand = new DelegateCommand(ExecuteAddClient, null);
            _clearCommand = new DelegateCommand(ExecuteClearCommand, null);
        }

        private void ExecuteClearCommand(object obj)
        {
            Name = string.Empty;
            Surname = string.Empty;
            PhoneNumber = string.Empty;
        }

        private void ExecuteAddClient(object obj)
        {
            if (StringVlalidationOperation.Execute(_name) && StringVlalidationOperation.Execute(_surname)
                && PhoneNumberValidationOperation.Execute(_phoneNumber))
            {
                InsertClientOperation.Execute(Name, Surname, PhoneNumber);
                ExecuteClearCommand(null);
            }
        }

        private void ExecuteGoBack(object obj)
        {
            Result = Result.ScheduleAnAppointmentView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(this.Name));
                    Name = StringValidationOperation.Execute(Name, true);
                }
            }
        }
        public string Surname
        {
            get { return _surname; }
            set
            {
                if (_surname != value)
                {
                    _surname = value;
                    OnPropertyChanged(nameof(this.Surname));
                    Surname = StringValidationOperation.Execute(Surname, true);
                }
            }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if (_phoneNumber != value)
                {
                    _phoneNumber = value;
                    OnPropertyChanged(nameof(this.PhoneNumber));
                }
            }
        }
        public long ClientId
        {
            get { return _clientId; }
            set
            {
                if (_clientId != value)
                {
                    _clientId = value;
                    OnPropertyChanged(nameof(this.ClientId));
                }
            }
        }
    }
}
