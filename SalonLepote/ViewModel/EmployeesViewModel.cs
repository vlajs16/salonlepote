﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class EmployeesViewModel : ViewModelBase
    {
        public event EventHandler GoToOtherPageEvents;
        private ObservableCollection<EmployeeViewModel> _employees;
        private EmployeeViewModel _selectedEmployee;
        
        private DelegateCommand _goToEmployeeDetails;
        private DelegateCommand _getAllEmployees;
        private DelegateCommand _deleteSelectedEmployee;
        private DelegateCommand _goBackToHome;
        private DelegateCommand _goToAddNewWorker;
        private Result _result;

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
            }
        }

        public EmployeesViewModel()
        {          
            _employees = new ObservableCollection<EmployeeViewModel>();
            _getAllEmployees = new DelegateCommand(ExecuteGetAllEmployees, null);
            _deleteSelectedEmployee = new DelegateCommand(ExecuteDeleteSelectedEmployee, null);
            _goToEmployeeDetails = new DelegateCommand(ExecuteGoToEmployeeDetails, null);
            _goBackToHome = new DelegateCommand(ExecuteGoBackToHome, null);
            _goToAddNewWorker = new DelegateCommand(ExecuteGoToAddNewEmployee, null);
            ExecuteGetAllEmployees(null);
        }

        private void ExecuteGoToAddNewEmployee(object obj)
        {
            Result = Result.AddNewWorkerView;
            GoToOtherPageEvents?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoBackToHome(object obj)
        {
            Result = Result.HomeView;
            GoToOtherPageEvents?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToEmployeeDetails(object obj)
        {
            Result = Result.EmployeeDetailsView;
            GoToOtherPageEvents?.Invoke(this, new EventArgs());
        }

        public ObservableCollection<EmployeeViewModel> Employees
        {
            get
            {
                return _employees;
            }
            set
            {
                if(value != _employees)
                {
                    _employees = value;
                    OnPropertyChanged(nameof(this.Employees));
                }
            }
        }

        public EmployeeViewModel SelectedEmoloyee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                if (value != _selectedEmployee)
                {
                    _selectedEmployee = value;
                    OnPropertyChanged(nameof(this.SelectedEmoloyee));
                }
            }
        }
        public DelegateCommand GetAllEmployees => _getAllEmployees;
        public DelegateCommand DeleteSelectedEmployee => _deleteSelectedEmployee;
        public DelegateCommand GoToEmployeeDetails => _goToEmployeeDetails;
        public DelegateCommand GoBackToHome => _goBackToHome;
        public DelegateCommand GoToAddNewWorker => _goToAddNewWorker;

        private void ExecuteGetAllEmployees(object obj)
        {         
            List<Employee> employees = GetAllEmployeesOperation.Execute();
            ObservableCollection<EmployeeViewModel> newEmployees = 
                new ObservableCollection<EmployeeViewModel>();
            foreach (var emp in employees)
            {
                EmployeeViewModel newEmp = new EmployeeViewModel
                {
                    EmployeeId = emp.EmployeeId,
                    Name = emp.Name,
                    Surname = emp.Surname
                };
                newEmployees.Add(newEmp);    
            }
            Employees = newEmployees;
        }

        private void ExecuteDeleteSelectedEmployee(object obj)
        {
            if(_selectedEmployee != null)
            {
                DeleteEmployeeOperation.Execute(_selectedEmployee.EmployeeId);
                _employees.Remove(_selectedEmployee);
            }
        }
    }
}
