﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    public class ProfessionsViewModel : ViewModelBase
    {
        
        private ObservableCollection<ProfessionViewModel> _professions;
        private ProfessionViewModel _selectedProfession;
        private Result _result;
        private DelegateCommand _getAllProfessions;
        private DelegateCommand _deleteSelectedProfession;

        private DelegateCommand _goToHomeView;
        private DelegateCommand _goToAddNewProfession;
        public event EventHandler GoToOtherPageEvent;

        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }

            }
        }

        public ProfessionsViewModel()
        {
            _professions = new ObservableCollection<ProfessionViewModel>();
            _getAllProfessions = new DelegateCommand(ExecuteGetAllProfessions,null);
            _deleteSelectedProfession = new DelegateCommand(ExecuteDeleteSelectedProfession, null);
            _goToHomeView = new DelegateCommand(ExecuteGoToHomePage, null);
            _goToAddNewProfession = new DelegateCommand(ExecuteGoToAddNewPageProfession, null);
            ExecuteGetAllProfessions(null);
        }

        private void ExecuteGoToAddNewPageProfession(object obj)
        {
            Result = Result.AddNewProfessionView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteGoToHomePage(object obj)
        {
            Result = Result.HomeView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        private void ExecuteDeleteSelectedProfession(object obj)
        {
            DeleteProfessionOperation.Execute((int)_selectedProfession.ProfessionId);
            _professions.Remove(_selectedProfession);
        }

        private void ExecuteGetAllProfessions(object obj)
        {
            List<Profession> professions = GetAllProfessionsOperation.Execute();
            ObservableCollection<ProfessionViewModel> newProfessions = 
                new ObservableCollection<ProfessionViewModel>();
            foreach (Profession prof in professions)
            {
                ProfessionViewModel profession = new ProfessionViewModel
                {
                    ProfessionId = prof.ProfessionId,
                    Name = prof.Name
                };
                newProfessions.Add(profession);
            }
            _professions = newProfessions;
        }

        public ProfessionViewModel SelectedProfession
        {
            get { return _selectedProfession; }
            set
            {
                if(_selectedProfession != value)
                {
                    _selectedProfession = value;
                    OnPropertyChanged(nameof(this.SelectedProfession));
                }
            }
        }

        public ObservableCollection<ProfessionViewModel> Professions
        {
            get
            { return _professions; }            
            set
            {
                if(_professions != value)
                {
                    _professions = value;
                    OnPropertyChanged(nameof(this.Professions));
                }
                
            }
        }

        public DelegateCommand GetAllProfessions => _getAllProfessions;
        public DelegateCommand DeleteSelectedProfession => _deleteSelectedProfession;
        public DelegateCommand GoToHomeViewCommand => _goToHomeView;
        public DelegateCommand GoToAddNewProfessionCommand => _goToAddNewProfession;

    }
}
