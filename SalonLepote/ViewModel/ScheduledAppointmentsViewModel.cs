﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Appointments;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ViewModel
{
    class SummaryAppointment : ViewModelBase
    {
        private Treatment _treatment;
        private Client _client;
        private DateTime _date;

        public SummaryAppointment()
        {

        }

        public SummaryAppointment(Treatment treatment, Client client, DateTime date)
        {
            Treatment = treatment;
            Client = client;
            Date = date;
        }

        public Treatment Treatment
        {
            get { return _treatment; }
            set
            {
                if(_treatment != value)
                {
                    _treatment = value;
                    OnPropertyChanged(nameof(this.Treatment));
                }
            }
        }

        public Client Client
        {
            get { return _client; }
            set
            {
                if (_client != value)
                {
                    _client = value;
                    OnPropertyChanged(nameof(this.Client));
                }
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged(nameof(this.Date));
                }
            }
        }

    }
    class ScheduledAppointmentsViewModel : ViewModelBase
    {
        public event EventHandler GoToOtherPageEvent;
        private ObservableCollection<EmployeeViewModel> _employees;
        private EmployeeViewModel _selectedEmployee;
        private ObservableCollection<SummaryAppointment> _appointments;
        private TreatmentViewModel _selectedTreatment;
        private DateTime _selectedDate;
        private Result _result;
        private DelegateCommand _goBackCommand;

        public DelegateCommand GoBackCommand => _goBackCommand;

        public ScheduledAppointmentsViewModel()
        {
            _employees = GenerateAllEmployees();
            _appointments = new ObservableCollection<SummaryAppointment>();
            _goBackCommand = new DelegateCommand(ExecuteGoBackCommand, null);
            SelectedDate = DateTime.Now;
        }

        private void GenerateSumarryAppointments()
        {
            List<Appointment> allAppointmetns = GetAppointmentsForSpecificDateAndEmployee.Execute((long)SelectedEmployee.EmployeeId, SelectedDate);
            if (allAppointmetns == null)
            {
                Appointments.Clear();
                return;
            }
            ObservableCollection<SummaryAppointment> newAppointments = new ObservableCollection<SummaryAppointment>(); 
            foreach (var app in allAppointmetns)
            {
                Appointment fullAppointment = GenerateFullAppointmentOperation.Execute(app);
                newAppointments.Add(new 
                    SummaryAppointment(fullAppointment.Worker.Expert.Treatment, fullAppointment.Client, fullAppointment.StartTime));
            }
            Appointments = newAppointments;
        }

        private ObservableCollection<EmployeeViewModel> GenerateAllEmployees()
        {
            List<Employee> allEmployees = GetAllEmployeesOperation.Execute();
            ObservableCollection<EmployeeViewModel> employees = new ObservableCollection<EmployeeViewModel>();
            foreach (var employee in allEmployees)
            {
                employees.Add(new EmployeeViewModel { EmployeeId = employee.EmployeeId, Name = employee.Name, Surname = employee.Surname });
            }
            return employees;
        }

        private void ExecuteGoBackCommand(object obj)
        {
            Result = Result.AppointmentsMainView;
            GoToOtherPageEvent?.Invoke(this, new EventArgs());
        }

        public DateTime SelectedDate
        {
            get
            {
                return _selectedDate;
            }
            set
            {
                if (value != _selectedDate)
                {
                    _selectedDate = value;
                    OnPropertyChanged(nameof(this.SelectedDate));
                    if(SelectedDate != null && SelectedEmployee != null)
                    {
                        GenerateSumarryAppointments();
                    }
                }
            }
        }
        public TreatmentViewModel SelectedTreatment
        {
            get
            {
                return _selectedTreatment;
            }
            set
            {
                if (value != _selectedTreatment)
                {
                    _selectedTreatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                }
            }
        }
        public EmployeeViewModel SelectedEmployee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                if (value != _selectedEmployee)
                {
                    _selectedEmployee = value;
                    OnPropertyChanged(nameof(this.SelectedEmployee));
                    if(SelectedDate!=null && SelectedEmployee!=null)
                        GenerateSumarryAppointments();
                }
            }
        }
        public ObservableCollection<SummaryAppointment> Appointments
        {
            get
            {
                return _appointments;
            }
            set
            {
                if (value != _appointments)
                {
                    _appointments = value;
                    OnPropertyChanged(nameof(this.Appointments));
                }
            }
        }
        public ObservableCollection<EmployeeViewModel> Employees
        {
            get
            {
                return _employees;
            }
            set
            {
                if (value != _employees)
                {
                    _employees = value;
                    OnPropertyChanged(nameof(this.Employees));
                }
            }
        }
        public Result Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(nameof(this.Result));
                }
            }
        }
    }
}
