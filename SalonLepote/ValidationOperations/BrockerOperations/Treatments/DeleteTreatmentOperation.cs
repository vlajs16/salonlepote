﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatments
{
    public class DeleteTreatmentOperation
    {
        public static void Execute(int id)
        {
            BrokerBaze.Broker.Delete(new Treatment() { TreatmentId = id });
        }
    }
}
