﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Treatments
{
    public class GetAllNonWorkingTreatmentsByProfessionIdAndEmployeeIdOperation
    {
        public static List<Treatment> Execute(long professionId, long employeeId)
        {
            string condition = $" (Id in (select TreatmentId from dbo.Expert where ProfessionId = {professionId}))  " +
                $" and (Id in (select TreatmentId from dbo.Expert where ProfessionId = {professionId})" +
                $" and Id not in (select TreatmentId from dbo.Worker where EmployeeId = {employeeId}))";
            Treatment treat = new Treatment();
            treat.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(treat).ConvertAll(x => (Treatment)x);
        }
    }
}
