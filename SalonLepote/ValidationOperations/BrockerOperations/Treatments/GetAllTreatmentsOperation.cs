﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements
{
    public class GetAllTreatmentsOperation
    {
        public static List<Treatment> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Treatment()).ConvertAll(x=>(Treatment)x);
        }
    }
}
