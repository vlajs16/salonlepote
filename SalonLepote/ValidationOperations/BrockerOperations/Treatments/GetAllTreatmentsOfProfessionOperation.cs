﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Treatments
{
    public class GetAllTreatmentsOfProfessionOperation
    {
        public static List<Treatment> Execute(long professionId)
        {
            string condition = $" Id in (select TreatmentId" +
                $" from dbo.Expert" +
                $" where ProfessionId = {professionId})";
            Treatment treatment = new Treatment();
            treatment.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(treatment).ConvertAll(x => (Treatment)x);
        }
    }
}
