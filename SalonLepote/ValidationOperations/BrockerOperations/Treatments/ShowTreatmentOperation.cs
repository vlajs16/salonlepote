﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements
{
    public class ShowTreatmentOperation
    {
        public static Treatment Execute(int id)
        {
            return (Treatment)BrokerBaze.Broker.GetObject(new Treatment() { TreatmentId = id });
        }
    }
}
