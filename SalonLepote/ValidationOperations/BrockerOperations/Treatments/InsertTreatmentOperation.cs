﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements
{
    public class InsertTreatmentOperation
    {
        public static void Execute(string name, int duration)
        {
            BrokerBaze.Broker.Insert(new Treatment()
            {
                TreatmentId = BrokerBaze.Broker.ReturnLastId(new Treatment()) + 1,
                Name = name,
                Duration = duration
            });
        }

    }
}
