﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatments
{
    public class UpdateTreatmentOperation
    {
        public static void Execute(Treatment treatment)
        {
            BrokerBaze.Broker.Update(treatment);
        }
    }
}
