﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Treatments
{
    public class GetAllTreatmentsOfWorkerOperation
    {
        public static List<Treatment> Execute(long employeeId, long professionId)
        {
            string condition = $"Id = (select TreatmentId" +
                $"form dbo.Work" +
                $"where EmployeeId = {employeeId} and ProfessionId = {professionId})";
            Treatment treat = new Treatment();
            treat.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(treat).ConvertAll(x => (Treatment)x);
        }
    }
}
