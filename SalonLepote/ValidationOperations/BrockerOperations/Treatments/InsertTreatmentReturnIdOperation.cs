﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Treatments
{
    public class InsertTreatmentReturnIdOperation
    {
        public static long Execute(string name, int duration)
        {
            long treatId = BrokerBaze.Broker.ReturnLastId(new Treatment()) + 1;
            BrokerBaze.Broker.Insert(new Treatment()
            {
                TreatmentId = treatId,
                Name = name,
                Duration = duration
            });
            return treatId;
        }
    }
}
