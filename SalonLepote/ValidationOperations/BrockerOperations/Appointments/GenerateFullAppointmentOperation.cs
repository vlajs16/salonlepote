﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Clients;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments
{
    public class GenerateFullAppointmentOperation
    {
        public static Appointment Execute(Appointment appointment)
        {
            appointment.Worker = GenerateFullWorkerOperation.Execute(appointment.Worker);
            appointment.Client = ShowClientOperation.Execute((int)appointment.Client.ClientId);
            return appointment;
        }
    }
}
