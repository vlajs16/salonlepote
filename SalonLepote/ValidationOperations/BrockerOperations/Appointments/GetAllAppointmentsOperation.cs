﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments
{
    public class GetAllAppointmentsOperation
    {
        public static List<Appointment> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Appointment()).ConvertAll(x=>(Appointment)x);
        }
    }
}
