﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments
{
    public class ShowAppointmentOperation
    {
        public static Appointment Execute(Appointment appointment)
        {
            return (Appointment)BrokerBaze.Broker.GetObject(appointment);
        }
    }
}
