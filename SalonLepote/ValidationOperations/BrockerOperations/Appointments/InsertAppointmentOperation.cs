﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments
{
    public class InsertAppointmentOperation
    {
        public static void Execute(Worker worker, Client client, DateTime startDate, DateTime endTime)
        {
            BrokerBaze.Broker.Insert(new Appointment { Worker = worker, Client = client,
                StartTime = startDate, EndTime = endTime});
        }
    }
}
