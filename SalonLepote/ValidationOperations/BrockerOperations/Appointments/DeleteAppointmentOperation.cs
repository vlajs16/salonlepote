﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Appointments
{
    public class DeleteAppointmentOperation
    {
        public static void Execute(Appointment appointment)
        {
            BrokerBaze.Broker.Delete(appointment);
        }
    }
}
