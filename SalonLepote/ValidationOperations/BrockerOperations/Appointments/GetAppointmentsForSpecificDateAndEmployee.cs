﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Appointments
{
    public class GetAppointmentsForSpecificDateAndEmployee
    {
        public static List<Appointment> Execute(long employeeId, DateTime date)
        {
            string condition = $"EmployeeId = {employeeId} and " +
                $"convert(varchar(10), StartDate,120) = '{DateStringConverter(date)}'";
            Appointment appointment = new Appointment();
            appointment.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(appointment).ConvertAll(x => (Appointment)x);
        }

        private static string DateStringConverter(DateTime date)
        {
            int day = date.Day;
            int year = date.Year;
            int month = date.Month;

            string dateString = $"{year}-";
            if (month < 10) dateString += "0";
            dateString += $"{month}-";
            if (day < 10) dateString += "0";
            dateString += $"{day}";
            return dateString;
        }
    }
}
