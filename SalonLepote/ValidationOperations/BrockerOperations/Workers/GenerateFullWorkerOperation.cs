﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Model;
using SalonLepote.ValidationOperations.BrockerOperations.Experts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers
{
    public class GenerateFullWorkerOperation
    {
        public static Worker Execute(Worker worker)
        {
            worker.Employee = ShowEmployeeOperation.Execute((int) worker.Employee.EmployeeId);
            worker.Expert = GenerateFullExpertOperation.Execute(worker.Expert);
            return worker;
        }
    }
}
