﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers
{
    public class InsertWorkerOperation
    {
        public static void Execute(Employee employee, Expert expert)
        {
            BrokerBaze.Broker.Insert(new Worker()
            {
                Employee = employee,
                Expert = expert
            });
        }

    }
}
