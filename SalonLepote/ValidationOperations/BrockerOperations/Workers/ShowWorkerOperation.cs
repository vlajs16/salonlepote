﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers
{
    public class ShowWorkerOperation
    {
        public static Worker Execute(Worker worker)
        {
            return (Worker)BrokerBaze.Broker.GetObject(worker);
        }
    }
}
