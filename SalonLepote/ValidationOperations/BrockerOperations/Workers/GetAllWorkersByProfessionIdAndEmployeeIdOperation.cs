﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Workers
{
    public class GetAllWorkersByProfessionIdAndEmployeeIdOperation
    {
        public static List<Worker> Execute(long professionId, long employeeId)
        {
            string condition = $"  ProfessionId = {professionId} and EmployeeId = {employeeId}";
            Worker worker = new Worker();
            worker.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(worker).ConvertAll(x => (Worker)x);
        }
    }
}
