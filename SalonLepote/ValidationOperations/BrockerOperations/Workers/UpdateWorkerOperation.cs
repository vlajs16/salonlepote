﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Workers
{
    class UpdateWorkerOperation
    {
        public static void Execute(Worker worker)
        {
            BrokerBaze.Broker.Update(worker);
        }
    }
}
