﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers
{
    public class DeleteWorkerOperation
    {
        public static void Execute(Worker worker)
        {
            BrokerBaze.Broker.Delete(worker);
        }
    }
}
