﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers
{
    public class GetAllWorkersOperation
    {
        public static List<Worker> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Worker()).ConvertAll(x => (Worker)x);
        }
    }
}
