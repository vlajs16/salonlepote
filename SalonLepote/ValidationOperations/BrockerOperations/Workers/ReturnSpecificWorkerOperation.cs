﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Workers
{
    public class ReturnSpecificWorkerOperation
    {
        public static Worker Execute(long employeeId, long treatmentId)
        {
            string condition = $"EmployeeId = {employeeId} and TreatmentId = {treatmentId}";
            Worker worker = new Worker();
            worker.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(worker).ConvertAll(x => (Worker)x).FirstOrDefault();
        }
    }
}
