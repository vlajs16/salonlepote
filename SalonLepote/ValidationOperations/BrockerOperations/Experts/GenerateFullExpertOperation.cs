﻿using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Treatements;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class GenerateFullExpertOperation
    {

        public static Expert Execute(Expert expert)
        {
            expert.Profession = ShowProfessionOperation.Execute((int)expert.Profession.ProfessionId);
            expert.Treatment = ShowTreatmentOperation.Execute((int)expert.Treatment.TreatmentId);
            return expert;
        }
    }
}
