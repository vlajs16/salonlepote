﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class DeleteExpertOperation
    {
        public static void Execute(Expert expert)
        {
            BrokerBaze.Broker.Delete(expert);
        }
    }
}
