﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class GetAllExpertsOperation
    {
        public static List<Expert> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Expert()).ConvertAll(x => (Expert)x);
        }
    }
}
