﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class UpdateExpertOperationOnlyProfession
    {
        public static void Execute(long professionId, long treatmentId)
        {
            string condition = $" TreatmentId = {treatmentId}";
            Expert exp = new Expert
            {
                Profession = new Profession { ProfessionId = professionId },
                Treatment = new Treatment { TreatmentId = treatmentId }
            };
            exp.SetSearchCondition(condition);
            BrokerBaze.Broker.UpdateSecond(exp);
        }
    }
}
