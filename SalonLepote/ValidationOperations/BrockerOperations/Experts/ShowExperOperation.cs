﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class ShowExperOperation
    {
        public static Expert Execute(Expert expert)
        {
            return (Expert)BrokerBaze.Broker.GetObject(expert);
        }
    }
}
