﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Experts
{
    public class InsertExpertOperation
    {
        public static void Execute(long professionId, long treatmentId)
        {
            BrokerBaze.Broker.Insert(new Expert {
                Profession = new Profession { ProfessionId = professionId },
                Treatment = new Treatment { TreatmentId = treatmentId}
            });
        }
    }
}
