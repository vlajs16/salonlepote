﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees
{
    public class UpdateEmployeeOperation
    {
        public static void Execute(Employee employee)
        {
            BrokerBaze.Broker.Update(employee);
        }
    }
}
