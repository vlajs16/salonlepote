﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Employees
{
    public class GetEmployeesByTreatmentOperation
    {
        public static List<Employee> Execute(long treatmentId)
        {
            string condition = $"Id in (SELECT EmployeeId" +
                $" FROM dbo.Worker WHERE TreatmentId = {treatmentId})";
            Employee emp = new Employee();
            emp.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(emp).ConvertAll(x => (Employee)x);
        }
    }
}
