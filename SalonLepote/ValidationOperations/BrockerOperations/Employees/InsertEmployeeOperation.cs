﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations
{
    public class InsertEmployeeOperation
    {
        public static void Execute(string name, string surname)
        {
            BrokerBaze.Broker.Insert(new Employee()
            {
                EmployeeId = BrokerBaze.Broker.ReturnLastId(new Employee()) + 1,
                Name = name,
                Surname = surname
            });
        }
    }
}
