﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees
{
    public class ShowEmployeeOperation
    {
        public static Employee Execute(int id)
        {
            return (Employee)BrokerBaze.Broker.GetObject(new Employee() { EmployeeId = id });
        }
    }
}
