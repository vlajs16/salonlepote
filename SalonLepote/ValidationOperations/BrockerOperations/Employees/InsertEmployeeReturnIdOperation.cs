﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations
{
    public class InsertEmployeeReturnIdOperation
    {
        public static long Execute(string name, string surname)
        {
            long id =  BrokerBaze.Broker.ReturnLastId(new Employee()) + 1;
            BrokerBaze.Broker.Insert(new Employee()
            {
                EmployeeId = id,
                Name = name,
                Surname = surname
            });
            return id;
        }
    }
}
