﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees
{
    public class DeleteEmployeeOperation
    {
        public static void Execute(long id)
        {
            BrokerBaze.Broker.Delete(new Employee() { EmployeeId = id });
        }
    }
}
