﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Employees
{
    public class GetAllEmployeesOperation
    {
        public static List<Employee> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Employee()).ConvertAll(x => (Employee)x);
        }
    }
}
