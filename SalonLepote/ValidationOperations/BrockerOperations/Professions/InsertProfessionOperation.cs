﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions
{
    public class InsertProfessionOperation
    {
        public static void Execute(string name)
        {
            BrokerBaze.Broker.Insert(new Profession()
            {
                ProfessionId = BrokerBaze.Broker.ReturnLastId(new Profession()) + 1,
                Name = name
            });
        }
    }
}
