﻿using SalonLepote.Konzolna;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Professions
{
    public class GetProfessionByTreatmentIdOperation
    {
        public static Profession Execute(long treatmentId)
        {
            string condition = $" Id in (select ProfessionId" +
                $" from dbo.Expert where TreatmentId = {treatmentId})";
            Profession prof = new Profession();
            prof.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(prof).ConvertAll(x => (Profession)x).FirstOrDefault();
        }
    }
}
