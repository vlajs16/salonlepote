﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions
{
    public class ShowProfessionOperation
    {
        public static Profession Execute(int id)
        {
            return (Profession)BrokerBaze.Broker.GetObject(new Profession() { ProfessionId = id });
        }
    }
}
