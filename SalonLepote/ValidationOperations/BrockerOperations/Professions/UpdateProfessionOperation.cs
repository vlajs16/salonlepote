﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions
{
    public class UpdateProfessionOperation
    {
        public static void Execute(Profession profession)
        {
            BrokerBaze.Broker.Update(profession);
        }
    }
}
