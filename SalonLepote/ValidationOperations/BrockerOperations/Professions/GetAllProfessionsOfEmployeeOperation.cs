﻿using SalonLepote.Konzolna;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions;
using SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Workers;
using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.BrockerOperations.Professions
{
    public class GetAllProfessionsOfEmployeeOperation
    {
        public static List<Profession> Execute(long employeeId)
        {
            string condition = $" Id in (select ProfessionId" +
                $" from dbo.Worker where EmployeeId = {employeeId})";
            Profession profession = new Profession();
            profession.SetSearchCondition(condition);
            return BrokerBaze.Broker.GetObjects(profession).ConvertAll(x => (Profession)x);
        }
    }
}
