﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions
{
    public class GetAllProfessionsOperation
    {
        public static List<Profession> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Profession()).ConvertAll(x => (Profession) x);
        }
    }
}
