﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Professions
{
    public class DeleteProfessionOperation
    {
        public static void Execute(int id)
        {
            BrokerBaze.Broker.Delete(new Profession() { ProfessionId = id });
        }
    }
}
