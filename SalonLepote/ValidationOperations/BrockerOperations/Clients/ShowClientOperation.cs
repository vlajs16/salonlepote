﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Clients
{
    public class ShowClientOperation
    {
        public static Client Execute(int id)
        {
            return (Client)BrokerBaze.Broker.GetObject(new Client() { ClientId = id });
        }
    }
}
