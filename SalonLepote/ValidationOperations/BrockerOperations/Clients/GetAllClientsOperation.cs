﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Clients
{
    public class GetAllClientsOperation
    {
        public static List<Client> Execute()
        {
            return BrokerBaze.Broker.GetObjects(new Client()).ConvertAll(x=>(Client)x);
        }
    }
}
