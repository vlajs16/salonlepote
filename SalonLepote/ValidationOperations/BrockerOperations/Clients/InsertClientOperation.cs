﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations
{
    public class InsertClientOperation
    {
        public static void Execute(string name, string surname, string phoneNumber)
        {
            BrokerBaze.Broker.Insert(new Client()
            {
                ClientId = BrokerBaze.Broker.ReturnLastId(new Client()) + 1,
                Name = name,
                Surname = surname,
                PhoneNumber = phoneNumber
            });
        }
    }
}
