﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.BrockerOperations.Clients
{
    public class UpdateClientOperation
    {
      public static void Execute(Client client)
        {
            BrokerBaze.Broker.Update(client);
        }
    }
}
