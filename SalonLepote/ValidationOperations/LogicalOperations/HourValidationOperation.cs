﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.LogicalOperations
{
    public class HourValidationOperation
    {
        public static bool Execute(int hour)
        {
            int startDay = 9;
            int endDay = 20;
            if (hour < startDay) return false;
            if (hour >= endDay) return false;
            return true;
        }
    }
}
