﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.LogicalOperations
{
    public class NumberValidationOperation
    {
        public static int Execute(string number)
        {
            if (number == null) return -1;
            if (number == string.Empty) return -1;
            int intNumber;
            if (!int.TryParse(number, out intNumber))
                return -1;
            if (intNumber < 15 || intNumber > 180) return -1;
            return intNumber;
        }
    }
}
