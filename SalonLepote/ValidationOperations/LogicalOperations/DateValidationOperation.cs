﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.LogicalOperations
{
    public class DateValidationOperation
    {
        public static bool Execute(int day,int month, int year)
        {
            DateTime today = DateTime.Now;
            if (year < today.Year) return false;
            if (month < today.Month) return false;
            if (day < today.Day) return false;
            return true;
        }
    }
}
