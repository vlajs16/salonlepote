﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.LogicalOperations
{
    public class CaseValidationOperation
    {
        public static string Execute(string name)
        {
            if (name == null) return null;
            if (name == string.Empty) return null;
            if (name.Length == 0) return null;
            string changedString = "";
            changedString += name[0].ToString().ToUpper();
            for (int i = 1; i < name.Length; i++)
            {
                changedString += name[i].ToString().ToLower();
            }
            return changedString;
        }
    }
}
