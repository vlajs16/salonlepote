﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.LogicalOperations
{
    public class GenerateEndTime
    {
        public static DateTime Execute(DateTime startTime, Treatment treatment)
        {
            return startTime.AddMinutes(treatment.Duration);
        }
    }
}
