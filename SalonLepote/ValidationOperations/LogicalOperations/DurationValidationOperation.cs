﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SalonLepote.ValidationOperations.LogicalOperations
{
    public class DurationValidationOperation : ValidationRule
    {
        public DurationValidationOperation()
        {
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int age = 0;

            try
            {
                if (((string)value).Length > 0)
                    age = Int32.Parse((String)value);
            }
            catch (Exception e)
            {
                return new ValidationResult(false, $"Illegal characters or {e.Message}");
            }

            if ((age < 0) || (age > 180))
            {
                return new ValidationResult(false,
                  $"Please enter an duration in the range: 0-180 min.");
            }
            return ValidationResult.ValidResult;
        }
    }
}
