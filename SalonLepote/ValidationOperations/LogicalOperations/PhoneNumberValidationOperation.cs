﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations
{
    public class PhoneNumberValidationOperation
    {
        public static bool Execute(string number)
        {
            return Regex.Match(number, @"^\+381\s6([0-9]{7}|[0-9]{8})$").Success;
        }
    }
}
