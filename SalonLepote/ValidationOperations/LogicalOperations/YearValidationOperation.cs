﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.LogicalOperations
{
    public class YearValidationOperation
    {
        public static bool Execute(int year)
        {
            if (DateTime.Now.Year > year) return false;
            return true;
        }
    }
}
