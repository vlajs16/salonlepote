﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations.LogicalOperations
{
    public class DayValidationOperation
    {
        public static bool Execute(int month, int day, int year)
        {
            if (day < 1 ) return false;
            if (day > 31) return false;
            switch (month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    {
                        if (day > 30) return false;
                        break;
                    }
                case 2:
                    {
                        if (year % 4 == 0 && day > 29) return false;
                        if (year % 4 != 0 && day > 28) return false;
                        break;
                    }
                default: break;
            }
            return true;
        }
    }
}
