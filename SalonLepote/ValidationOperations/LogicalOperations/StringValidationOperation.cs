﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.ValidationOperations.LogicalOperations
{
    public class StringValidationOperation
    { 
        public static string Execute(string name, bool nameCheck)
        {
            if (name == null) return null;
            if (name.Equals(string.Empty)) return null;
            if (name.Length <= 1) return null;
            bool check = false;
            string[] strings = name.Split(' ');
            string newStrings = "";
            foreach (var str in strings)
            {
                if (str!=null && str!= string.Empty && str != " ")
                {
                    if (nameCheck == true || (nameCheck == false && check == false))
                    {
                        newStrings += CaseValidationOperation.Execute(str) + " ";
                        check = true;
                    }
                    else
                    {
                        newStrings += str.ToLower() + " ";
                    }
                }
            }
            return newStrings.Substring(0, newStrings.Length-1);
        }
    }
}
