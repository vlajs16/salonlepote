﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna.ValidationOperations
{
    public class StringVlalidationOperation
    {
        public static bool Execute(string name)
        {
            if (name == null) return false;
            if (name.Equals(string.Empty)) return false;
            if (name.Length <= 1) return false;
            foreach (var item in name)
            {
                if (!Char.IsLetter(item) && item != ' ')
                    return false;
            }
            return true;
        }
    }
}
