﻿using SalonLepote.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Konzolna
{
    public class BrokerBaze
    {
        private SqlConnection _connetion;
        private static BrokerBaze _broker;

        private BrokerBaze()
        {
            _connetion = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BeautySalon;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        public static BrokerBaze Broker
        {
            get
            {
                if (_broker == null)
                    _broker = new BrokerBaze();
                return _broker;
            }
        }

        private void OpenConnection()
        {
            _connetion.Open();
        }

        private void CloseConnection()
        {
            _connetion.Close();
        }

        public void Insert(AbstractModel obj)
        {
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"insert into {obj.GetTableName()}({obj.GetColumnName()}) values ({obj.GetColumnValues()})";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return;
                throw new Exception("Insertion failed.");
            }
            catch(Exception ex)
            {
                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(AbstractModel obj)
        {
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"update {obj.GetTableName()} set {obj.GetChangeQuery()} {obj.GetSearchCondition()}";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return;
                throw new Exception("Update failed.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public void UpdateSecond(AbstractModel obj)
        {
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"update {obj.GetTableName()} set {obj.GetChangeQuery()} {obj.SearchCondition}";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return;
                throw new Exception("Update failed.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(AbstractModel obj)
        {
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"delete from {obj.GetTableName()} {obj.GetSearchCondition()}";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return;
                throw new Exception("Delete failed.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<AbstractModel> GetObjects(AbstractModel obj)
        {
            SqlDataReader reader = null;
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"select * from {obj.GetTableName()} {obj.SearchCondition}";
                command.CommandText = query;
                reader = command.ExecuteReader();
                return obj.CreateObjects(reader);
            }
            catch (Exception ex)
            {

                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public AbstractModel GetObject(AbstractModel obj)
        {
            SqlDataReader reader = null;
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"select * from {obj.GetTableName()} {obj.GetSearchCondition()}";
                command.CommandText = query;
                reader = command.ExecuteReader();
                return obj.CreateObjects(reader).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }

        public long ReturnLastId(AbstractModel obj)
        {
            SqlDataReader reader = null;
            SqlCommand command = null;
            try
            {
                OpenConnection();
                command = _connetion.CreateCommand();
                string query = $"select max({obj.GetIdColumnName()}) from {obj.GetTableName()}";
                command.CommandText = query;
                reader = command.ExecuteReader();
                long id = 0;
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        if (reader[0].GetType() != id.GetType()) break;
                        id = (long)reader[0];
                    }
                
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error! {ex.Message}");
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}
