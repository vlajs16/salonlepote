﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Employee : AbstractModel
    {
        private long _employeeId;
        private string _name;
        private string _surname;


        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public long EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Employee employee = new Employee()
                {
                    EmployeeId = (long)reader["Id"],
                    Name = reader["Name"].ToString(),
                    Surname = reader["Surname"].ToString()
                };
                objects.Add(employee);
            }
            return objects;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Employee employee = (Employee)obj;
            if (employee.EmployeeId != _employeeId) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"Name='{Name}', Surname = '{Surname}'";
        }

        public override string GetColumnName()
        {
            return "Id, Name, Surname";
        }

        public override string GetColumnValues()
        {
            return $"{EmployeeId},'{Name}','{Surname}'";
        }

        public override string GetIdColumnName()
        {
            return "Id";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"Id={EmployeeId}");
        }

        public override string GetTableName()
        {
            return "dbo.Employee";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return _employeeId + ". " +_name + " " + _surname;
        }
    }
}
