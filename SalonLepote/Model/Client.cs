﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Client : AbstractModel
    {
        private long _clientId;
        private string _name;
        private string _surname;
        private string _phoneNumber;


        public long ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }


        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Client client = new Client()
                {
                    ClientId = (long)reader["Id"],
                    Name = reader["Name"].ToString(),
                    Surname = reader["Surname"].ToString(),
                    PhoneNumber = reader["PhoneNumber"].ToString()
                };
                objects.Add(client);
            }
            return objects;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Client client = (Client)obj;
            if (client.ClientId != _clientId) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"Name='{Name}', Surname = '{Surname}', PhoneNumber = '{PhoneNumber}'";
        }

        public override string GetColumnName()
        {
            return "Id, Name, Surname, PhoneNumber";
        }

        public override string GetColumnValues()
        {
            return $"{ClientId}, '{Name}', '{Surname}', '{PhoneNumber}'";
        }

        public override string GetIdColumnName()
        {
            return "Id";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"Id={ClientId}");
        }

        public override string GetTableName()
        {
            return "dbo.Client";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return _clientId+ " "+_name + " " + _surname + " " + _phoneNumber;
        }
    }
}
