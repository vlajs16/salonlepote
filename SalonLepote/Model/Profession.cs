﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Profession : AbstractModel
    {
        private long _professionId;
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public long ProfessionId
        {
            get { return _professionId; }
            set { _professionId = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Profession profession = new Profession()
                {
                    ProfessionId = (long)reader["Id"],
                    Name = reader["Name"].ToString()
                };
                objects.Add(profession);
            }
            return objects;
        }


        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Profession profession = (Profession)obj;
            if (profession.ProfessionId != _professionId) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"Name='{Name}'";
        }

        public override string GetColumnName()
        {
            return "Id, Name";
        }

        public override string GetColumnValues()
        {
            return $"{_professionId}, '{_name}'";
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            throw new NotImplementedException();
            return base.GetHashCode();
        }

        public override string GetIdColumnName()
        {
            return "Id";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"Id={_professionId}");
        }

        public override string GetTableName()
        {
            return "dbo.Profession";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return _professionId + " " +_name;
        }
    }
}
