﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public abstract class AbstractModel
    {
        private string _searchCondition = string.Empty;

        public string SearchCondition => _searchCondition;

        public string SetSearchCondition(string condition)
        {
            _searchCondition = $"WHERE {condition}";
            return _searchCondition;
        }

        public abstract string GetTableName();
        public abstract string GetColumnName();
        public abstract string GetColumnValues();
        public abstract string GetChangeQuery();

        public abstract string GetSearchCondition();
        public abstract string GetIdColumnName();
        public abstract List<AbstractModel> CreateObjects(SqlDataReader reader);
        public abstract long IdExecuter(SqlDataReader reader);
    }
}
