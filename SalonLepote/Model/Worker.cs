﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Worker : AbstractModel
    {
        private Employee _employee;
        private Expert _expert;

        public Employee Employee
        {
            get { return _employee; }
            set { _employee = value; }
        }
        public Expert Expert
        {
            get { return _expert; }
            set { _expert = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Worker worker = new Worker()
                {
                    Employee = new Employee()
                    {
                        EmployeeId = (long)reader["EmployeeId"]
                    },
                    Expert = new Expert()
                    {
                        Profession = new Profession()
                        {
                            ProfessionId = (long)reader["ProfessionId"]
                        },
                        Treatment = new Treatment()
                        {
                            TreatmentId = (long)reader["TreatmentId"]
                        }
                    }
                };
                objects.Add(worker);
                

            }
            return objects;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Worker worker = (Worker)obj;
            if (worker.Employee != _employee) return false;
            if (worker.Expert != _expert) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"EmployeeId = {_employee.EmployeeId}, ProfessionId = {_expert.Profession.ProfessionId}, TreatmentId = {_expert.Treatment.TreatmentId}";
        }

        public override string GetColumnName()
        {
            return "EmployeeId, ProfessionId, TreatmentId";
        }

        public override string GetColumnValues()
        {
            return $"{_employee.EmployeeId}, {_expert.Profession.ProfessionId}, {_expert.Treatment.TreatmentId}";
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            throw new NotImplementedException();
            return base.GetHashCode();
        }

        public override string GetIdColumnName()
        {
            return "EmployeeId, ProfessionId, TreatmentId";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"EmployeeId={_employee.EmployeeId} and ProfessionId={_expert.Profession.ProfessionId} and TreatmentId = {_expert.Treatment.TreatmentId}");
        }

        public override string GetTableName()
        {
            return "dbo.Worker";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return $"{_employee.ToString()}, {_expert.ToString()}";
        }
    }
}
