﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Expert : AbstractModel
    {
        private Treatment _treatment;
        private Profession _profession;

        public Treatment Treatment
        {
            get { return _treatment; }
            set { _treatment = value; }
        }
        public Profession Profession
        {
            get { return _profession; }
            set { _profession = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Expert expert = new Expert()
                {
                    Treatment = new Treatment()
                    {
                        TreatmentId = (long)reader["TreatmentId"]
                    },
                    Profession = new Profession()
                    {
                        ProfessionId = (long)reader["ProfessionId"]
                    }
                };
                objects.Add(expert);


            }
            return objects;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Expert expert = (Expert)obj;
            if (expert.Treatment != _treatment) return false;
            if (expert.Profession != _profession) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"ProfessionId = {_profession.ProfessionId}, TreatmentId = {_treatment.TreatmentId}";
        }

        public override string GetColumnName()
        {
            return "ProfessionId, TreatmentId";
        }

        public override string GetColumnValues()
        {
            return $"{_profession.ProfessionId}, {_treatment.TreatmentId}";
        }

        public override string GetIdColumnName()
        {
            return "ProfessionId, TreatmentId";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"TreatmentId={_treatment.TreatmentId} and ProfessionId={_profession.ProfessionId}");
        }

        public override string GetTableName()
        {
            return "dbo.Expert";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return $"{_profession.ToString()}, {_treatment.ToString()}";
        }
    }
}
