﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Appointment : AbstractModel
    {
        private Worker _worker;
        private Client _client;
        private DateTime _startTime;
        private DateTime _endTime;

        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }


        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }


        public Client Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public Worker Worker
        {
            get { return _worker; }
            set { _worker = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Appointment appointment = new Appointment()
                {
                    Worker = new Worker()
                    {
                        Employee = new Employee { EmployeeId = (long)reader["EmployeeId"] },
                        Expert = new Expert
                        {
                            Profession = new Profession { ProfessionId = (long)reader["ProfessionId"] },
                            Treatment = new Treatment { TreatmentId = (long)reader["TreatmentId"] }
                        }
                    },             
                    Client = new Client { ClientId = (long)reader["ClientId"] },
                    StartTime = (DateTime) reader["StartDate"],
                    EndTime = (DateTime) reader["EndTime"]
                };
                objects.Add(appointment);
            }
            return objects;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Appointment appointment = (Appointment)obj;
            if (appointment.Worker != _worker) return false;
            if (appointment.Client != _client) return false;
            if (appointment.StartTime != _startTime) return false;

            return true;
        }

        public override string GetChangeQuery()
        {
            return "";
        }

        public override string GetColumnName()
        {
            return "EmployeeId, ProfessionId, TreatmentId, ClientId, StartDate, EndTime";
        }

        public override string GetColumnValues()
        {
            return $"{_worker.Employee.EmployeeId}, {_worker.Expert.Profession.ProfessionId}," +
                $"{_worker.Expert.Treatment.TreatmentId}, {_client.ClientId}, '{_startTime}', '{_endTime}'";
        }

        public override string GetIdColumnName()
        {
            return "EmployeeId, ProfessionId, TreatmentId, ClientId, StartDate";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"EmployeeId={_worker.Employee.EmployeeId} and ProfessionId={_worker.Expert.Profession.ProfessionId}" +
                $" and TreatmentId = {_worker.Expert.Treatment.TreatmentId} and ClientId = {_client.ClientId} " +
                $"and StartDate = '{_startTime}'");
        }

        public override string GetTableName()
        {
            return "dbo.Appointment";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return $"{Worker.ToString()}, {_client.ToString()}" +
                $"{_startTime.ToString("MM/dd/yyyy H:mm")} h, {_endTime.ToString("H:mm")} h";
        }
    }
}
