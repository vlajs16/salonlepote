﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonLepote.Model
{
    public class Treatment : AbstractModel
    {
        private long _treatmentId;
        private string _name;
        private int _duration;

        public int Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public long TreatmentId
        {
            get { return _treatmentId; }
            set { _treatmentId = value; }
        }

        public override List<AbstractModel> CreateObjects(SqlDataReader reader)
        {
            List<AbstractModel> objects = new List<AbstractModel>();
            while (reader.Read())
            {
                Treatment treatment = new Treatment()
                {
                    TreatmentId = (long)reader["Id"],
                    Name = reader["Name"].ToString(),
                    Duration = (int)reader["Durability"]
                };
                objects.Add(treatment);
            }
            return objects;
        }


        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Treatment treatment = (Treatment)obj;
            if (treatment.TreatmentId != _treatmentId) return false;
            return true;
        }

        public override string GetChangeQuery()
        {
            return $"Name = '{_name}', Durability = {_duration}";
        }

        public override string GetColumnName()
        {
            return "Id, Name, Durability";
        }

        public override string GetColumnValues()
        {
            return $"{_treatmentId}, '{_name}', {_duration}";
        }

        public override string GetIdColumnName()
        {
            return "Id";
        }

        public override string GetSearchCondition()
        {
            return SetSearchCondition($"Id={_treatmentId}");
        }

        public override string GetTableName()
        {
            return "dbo.Treatment";
        }

        public override long IdExecuter(SqlDataReader reader)
        {
            return 0;
        }

        public override string ToString()
        {
            return _treatmentId + " " + _name + " " + _duration + " min";
        }
    }
}
